if exists("b:current_syntax")
  finish
endif

" Symbol
syn match BisSymbol '\v[_A-Za-z][_A-za-z0-9]*'

" Directive
syn match BisDirective '\v\.[_A-Za-z][_A-za-z0-9]*'

" Literal numbers
syn match BisNumber '\v-?[0-9]+'
syn match BisNumber '\v-?0b[01]+'
syn match BisNumber '\v-?0x[0-9A-Za-z]+'

" Quoted string
syntax region BisString start='\v"' skip='\v\\.' end='\v"'

" Label
syn match BisLabel '\v[_A-Za-z][_A-za-z0-9]*:'

" Line comments
syn region BisComment start=';' end='\n'

" Instructions
syn keyword BisInstruction
\ add addc addi and andi break call divs divsi divu divui icall ijmp jmp
\ load16 load32 load64 load8 loadi16 loadi32 loadi64 loadi8 mods modsi modu
\ modui movbit muls mulsi mulu mului nop not or ori pop push rcall rems remsi
\ ret reti rjmp rotl rotli rotr rotri shiftl shiftli shiftr shiftri sleep
\ store16 store32 store64 store8 storei16 storei32 storei64 storei8 sub subc
\ subi swap syscall undefined undefined undefined xor xori

" Jump/call conditions
syn keyword BisCondition n nc nn no nz o seg sel sgr sle ueg uel ugr ule unc z

" Special purpose registers
syn keyword BisRegister mar mdr pc irh irl sp fp slr fr zr im cc ext3 ext2
\ ext1 ext0

" General purpose registers
syn match BisRegister '\vr([0-9]|[12][0-9]|3[01])'

let b:current_syntax = 'bis'

" highlight BisInstruction  ctermfg=9   ctermbg=none  cterm=bold
" highlight BisCondition    ctermfg=3   ctermbg=none
" highlight BisRegister     ctermfg=11  ctermbg=none
" highlight BisDirective    ctermfg=2   ctermbg=none
" highlight BisNumber       ctermfg=13  ctermbg=none
" highlight BisString       ctermfg=6   ctermbg=none
" highlight BisLabel        ctermfg=   ctermbg=none
" highlight BisSymbol       ctermfg=12  ctermbg=none
" highlight BisComment      ctermfg=7   ctermbg=none

highlight link BisInstruction Keyword
highlight link BisCondition   Conditional
highlight link BisRegister    Type
highlight link BisDirective   Function
highlight link BisNumber      Number
highlight link BisString      StorageClass
highlight link BisLabel       Label
highlight link BisSymbol      Identifier
highlight link BisComment     Comment
