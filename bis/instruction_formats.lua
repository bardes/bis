local formats = {
    R0 = {type = 'Register', args = 0, size = 4},
    R1 = {type = 'Register', args = 1, size = 4},
    R2 = {type = 'Register', args = 2, size = 4},
    R3 = {type = 'Register', args = 3, size = 4},
    R4 = {type = 'Register', args = 4, size = 4},

    I1 = {type = 'Immediate', args = 1, size = 8},
    I2 = {type = 'Immediate', args = 2, size = 8},
    I3 = {type = 'Immediate', args = 3, size = 8},
    I4 = {type = 'Immediate', args = 4, size = 8},
    I5 = {type = 'Immediate', args = 5, size = 8},

    J  = {type = 'Jump/Call', size = 8},
    RJ = {type = 'Jump/Call', size = 4},
    IJ = {type = 'Jump/Call', size = 4},

    M  = {type = 'MovBit', size = 4}
}

return formats
