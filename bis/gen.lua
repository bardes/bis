#!/usr/bin/env lua5.3
-- Utilitary used to generate files from the architecture description

-- Adds the file path to the package search path
local file_path = debug.getinfo(1, "S").source:match("@(.*/)") or './'
package.path = table.concat {
    file_path, '../bis/?.lua;';
    file_path, '?.lua;';
    package.path
}

local arch = require 'arch'
local utils = require 'utils'
local effect = require 'effect'

local argparse = require 'argparse'
local molde = require 'molde'

local files = {
    registers = {
        description = 'Register definitions containing:\n' ..
        'the SPRs IDs, flag register bits and the registes ammounts.'
    },

    instructions = {
        description = 'Opcodes and other instruction definitions.'
    },

    instruction_prototypes = {
        description = 'Prototypes of each instruction\'s implementation.'
    },

    instruction_table = {
        description = 'Table of pointers to the implementation' ..
        ' of each instruction, indexed by opcode.'
    },

    conditions = {
        description = 'Condition bits definitions.'
    },

    gen_instruction_table = {
        description = 'LaTeX version of the instruction table used on the' ..
        ' manual as a reference.'
    },

    gen_instruction_reference = {
        description = 'Instruction reference describing every instruction' ..
        'in detail.'
    }
}

function files.registers.gen()
    print(('/* %s */\n'):format(files.registers.description))

    -- Register ammounts
    print(("#define BIS_NGPRS %dU"):format(arch.registers.n_gprs))
    print(("#define BIS_NSPRS %dU"):format(arch.registers.n_sprs))
    print(("#define BIS_NREGS %dU"):format(arch.registers.n_regs))
    print()

    -- Register names
    for mnemonic, reg in pairs(arch.registers.spr.byMnemonic) do
        print(("#define BIS_%s_REG ((bis_register_id_t) 0x%XU)")
            :format(mnemonic:upper(), reg.id))
    end
    print();

    -- Flag register bits
    for mnemonic, bit in pairs(arch.registers.flag_register) do
        print(("#define BIS_FR_%s %dU /* %s */")
            :format(mnemonic:upper(), bit.position, bit.name))
    end
end

local function get_inst_size(inst)
    local inst_type = arch.instructions.formats[inst.format]
    if type(inst_type) == 'table' and type(inst_type.size) == 'number' then
        return tostring(inst_type.size) .. 'U'
    else
        io.stderr:write(("Warning instruction %s has a bad type: %s.\n")
        :format(inst.mnemonic, utils.sinspect(inst.type)))
        return ("#error %s size is undefined!"):format(inst.mnemonic)
    end
end

function files.instructions.gen()
    print(('/* %s */\n'):format(files.instructions.description))

    for mnemonic, inst in pairs(arch.instructions.byMnemonic) do
        print(("#define BIS_%s_OP ((bis_opcode_t) 0x%XU)")
            :format(mnemonic:upper(), inst.opcode))
        print(("#define BIS_%s_CYCLES %dU")
            :format(mnemonic:upper(), inst.cycles))
        print(("#define BIS_%s_SIZE %s")
            :format(mnemonic:upper(), get_inst_size(inst)))
    end
    print()

    print(("#define BIS_NINSTS %dU"):format(arch.instructions.n_insts))

    print()
    print('typedef enum {')
    for _, f, _ in utils.sorted_pairs(arch.instructions.formats) do
        print(('    BIS_%s_FORMAT,'):format(f))
    end
    print('    BIS_UNKNOWN_FORMAT\n} bis_instruction_format_t;')
end

function files.instruction_prototypes.gen()
    print(('/* %s */\n'):format(files.instruction_prototypes.description))

    for mnemonic, _ in pairs(arch.instructions.byMnemonic) do
        print(("BIS_INSTRUCTION(%s);"):format(mnemonic))
    end
end

function files.instruction_table.gen()
    print(('/* %s */\n'):format(files.instruction_table.description))

    local opcode = 0
    local mnemonic, size, format
    repeat
        local instruction = arch.instructions.byOpcode[opcode]
        mnemonic = instruction and instruction.mnemonic or 'undefined'
        size = instruction and instruction.size or 0
        format = instruction and instruction.format or 'UNKNOWN'

        print(("/* 0x%X */ {%q, bis_%s_inst, %d, BIS_%s_FORMAT},")
        :format(opcode, mnemonic, mnemonic, size, format))

        opcode = opcode + 1
        if opcode % 16 == 0 then print() end
    until opcode == arch.instructions.n_insts - 1
    print(("/* 0x%X */ {%q, bis_%s_inst, %d, BIS_%s_FORMAT},")
    :format(opcode, mnemonic, mnemonic, size, format))
end

function files.conditions.gen()
    print(('/* %s */\n'):format(files.conditions.description))

    for mnemonic, condition in pairs(arch.conditions.byMnemonic) do
        print(("#define BIS_%s_COND ((bis_condition_t) 0x%XU) /* %s */")
            :format(mnemonic:upper(), condition.code, condition.name))
    end
end

function files.gen_instruction_table.gen()
    print('TODO');
end

local function generate_substitution_table(instruction, opcode)
    local s_table = utils.shallow_copy(instruction)
    local format = arch.instructions.formats[instruction.format]

    s_table.type = format.type
    s_table.size = format.size

    s_table.opcode_hex = ('%02X'):format(opcode)
    s_table.opcode_binh = utils.to_bin((opcode & 0x7F) >> 4, 3)
    s_table.opcode_binl = utils.to_bin(opcode & 0xF, 4)

    if #s_table.status_register == 0 then
        s_table.status_register = '[not modified]'
    else
        s_table.status_register = table.concat(s_table.status_register, ', ')
    end

    s_table.description = utils.latex_escape(utils.trim(s_table.description))

    s_table.effect = effect:match(utils.trim(s_table.effect))

    if s_table.example then
        s_table.example = utils.trim(s_table.example)
    end

    return s_table
end

function files.gen_instruction_reference.gen(args)
    -- Checks for the template option
    if args == nil or args.template == nil then
        io.stderr:write('gen_instruction_reference needs a tamplate file ' ..
        'given via the --template option.\n')
        os.exit(1)
    end

    -- Reads the file
    local template_file, err = io.open(args.template, 'r')
    if err then io.stderr:write(err .. '\n'); os.exit(1) end
    local template = molde.load(template_file:read('*all'))

    -- Separates the instructions by class
    local instructions_by_class = {}
    for opcode, instruction in pairs(arch.instructions.byOpcode) do
        if instructions_by_class[instruction.class] == nil then
            instructions_by_class[instruction.class] = {}
        end
        instructions_by_class[instruction.class][opcode] = instruction
    end

    -- Generates the output
    for i, class in ipairs(arch.instructions.classes) do
        -- Prints the section header
        if(i > 1) then io.stdout:write([[\newpage]]) end
        print(([[\section{%s}]]):format(class.pretty_name))

        -- Prints each instruction's content
        for _, opcode, instruction
        in utils.sorted_pairs(instructions_by_class[class.name]) do
            local s = template(generate_substitution_table(instruction, opcode))
            print(s)
        end
    end
end

local valid_files = {}
for _, file, _ in utils.sorted_pairs(files) do
    table.insert(valid_files, file)
end
valid_files = utils.break_string(table.concat(valid_files, ', '), 60)

local parser = argparse('gen.lua', 'Include data generator.')
parser:argument('file', 'Valid options are:\n' .. valid_files)
parser:option('-t --template', 'Template file used to generate the output.')
local args = parser:parse()

if files[args.file] then
    files[args.file].gen(args)
else
    io.stderr:write(('Unknown file to generate: %q\n'):format(args.file))
    os.exit(1)
end
