return {
    {name = 'arithmetic', pretty_name = 'Arithmetic'},
    {name = 'logical', pretty_name = 'Logical'},
    {name = 'load_store', pretty_name = 'Load / Store'},
    {name = 'flow_control', pretty_name = 'Flow Control'},
    {name = 'misc', pretty_name = 'Misc'}
}
