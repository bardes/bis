local inspect = require 'inspect'
local lpeg = require 'lpeglabel'

-- The module table
local utils = {}

-- Prints a table in a human readable format
function utils.inspect(thing)
    print(inspect(thing))
end

utils.sinspect = inspect

local function iter(list, i)
    local i = i + 1
    local entry = list[i]
    if entry then return i, entry.key, entry.value end
end

function utils.sorted_pairs(elements, comp)
    comp = comp or function(a, b) return a.key < b.key end

    local s = {}
    for key, value in pairs(elements) do
        table.insert(s, {['key'] = key, ['value'] = value})
    end
    table.sort(s, comp)

    return iter, s, 0
end

function utils.shallow_copy(t)
    if type(t) ~= "table" then return t end
    local meta = getmetatable(t)
    local target = {}
    for k, v in pairs(t) do target[k] = v end
    setmetatable(target, meta)
    return target
end

-- Reindxes a table based on a new index key
function utils.reindex(target, new_index, old_index)
    local r = {}
    for k, v in pairs(target) do
        r[v[new_index]] = utils.shallow_copy(v)
        r[v[new_index]][old_index] = k
        r[v[new_index]][new_index] = nil
    end
    return r
end

-- Merges a set of tables
function utils.merge(tables)
    local merged = {}
    for t, content in ipairs(tables) do
        for key, value in pairs(content) do
            merged[key] = value
        end
    end
    return merged
end

-- Breaks a long string (at white spaces) into smaller lines
-- Adapted from: https://stackoverflow.com/a/15809276
function utils.break_string(str, max_line_length)
   local lines = {}
   local line
   str:gsub('(%s*)(%S+)',
      function(spc, word)
         if not line or #line + #spc + #word > max_line_length then
            table.insert(lines, line)
            line = word
         else
            line = line..spc..word
         end
      end
   )
   table.insert(lines, line)
   return table.concat(lines, '\n')
end

-- Converts an integer into a binary string
function utils.to_bin(n, len)
    local bits = {}
    len = len or 0
    while n > 0 do
        table.insert(bits, n % 2 == 0 and '0' or '1')
        n = n >> 1
    end
    while #bits < len do table.insert(bits, '0') end
    return string.reverse(table.concat(bits))
end

-- lpeg pattern that trims spaces before a line starts
local trim = lpeg.Cs(
   (lpeg.P' '^0 / '') *
   ((lpeg.P'\n' * lpeg.P' '^0) / '\n' + 1)^0
)

function utils.trim(str)
    return trim:match(str)
end

local latex_escape_table = {
    ['~'] = [[\textasciitilde{}]],
    ['^'] = [[\textasciicircum{}]],
    ['\\'] = [[\textbackslash{}]]
}
local latex_escape = lpeg.C(lpeg.S'&%$#_{}') / '\\%0'
                   + lpeg.C(lpeg.S'~^\\') / latex_escape_table
                   + 1
latex_escape = lpeg.Cs(latex_escape^0)

function utils.latex_escape(str)
    return latex_escape:match(str)
end

return utils
