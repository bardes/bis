local utils = require "utils"

local instructions = {
    -- Valid instruction classes
    classes = require 'classes',

    -- Valid instruction formats
    formats = require 'instruction_formats',

    -- Opcode length in bits
    opcode_length = 7
}

-- Maximum number of instructions
instructions.n_insts = 2 ^ instructions.opcode_length

instructions.byMnemonic = utils.merge {
    require('instructions/arithmetic'),
    require('instructions/flow_control'),
    require('instructions/load_store'),
    require('instructions/logical'),
    require('instructions/misc')
}

instructions.byOpcode =
    utils.reindex(instructions.byMnemonic, 'opcode', 'mnemonic')

return instructions
