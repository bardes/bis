-- FIXME: This grammar is evil and confusing. A proper grammar (i.e. a grammar
-- that generates a parsing tree) should replace it

local utils = require 'utils'
local arch = require 'arch'

local lpeg = require 'lpeglabel'

-- TODO Avoid repaeating this lpeg boilerplate
-- Some lpeg shortcuts
local P = lpeg.P; local S = lpeg.S; local R = lpeg.R; local C = lpeg.C;
local Cs = lpeg.Cs; local Cc = lpeg.Cc; local V = lpeg.V
local alnum = R('az', 'AZ', '09')
local sp = S' \t'

-- Registers
local GPR = C('r' * ('3' * S'01' + S'12' * R'09' + R'09' + S'xyzw')) * (-alnum)
local SPR = P(1) - P(1)
for _, mnemonic, _ in utils.sorted_pairs(arch.registers.spr.byMnemonic) do
    SPR = C(mnemonic) * (-alnum) + SPR
end

-- Immediate values
local CONSTS = C(P'address' + P'immediate' + P'condition' + P'size' + P 'cycles')
for _, mnemonic, _ in utils.sorted_pairs(arch.registers.flag_register) do
    CONSTS = C(mnemonic) * (-alnum) + CONSTS
end

-- Operators
local operator_table = {
    ['<-'] = [[\(\leftarrow\)]],
    ['+'] = [[\(+\)]],
    ['-'] = [[\(-\)]],
    ['/'] = [[\(\div\)]],
    ['*'] = [[\(\times\)]],
    ['<<'] = [[\(\verb|<<|\)]],
    ['>>'] = [[\(\verb|>>|\)]],
    ['xor'] = [[\(\oplus\)]],
    ['or'] = [[\(\lor\)]],
    ['and'] = [[\(\land\)]],
    ['not '] = [[\(\lnot\)]]
}
local OP = P(1) - P(1)
for _, operator, _ in utils.sorted_pairs(operator_table) do
    OP = C(operator) + OP
end

function special(p)
    return ([[\textbf{%s}]]):format(p)
end

function constant(p)
    return ([[\textit{%s}]]):format(p)
end

local indent_begin = Cc('\n' .. [[\begin{addmargin}[5mm]{0mm}]] .. '\n')
local indent_end = Cc('\n' .. [[\end{addmargin}]] .. '\n')


local effect = P {'EFFECTS';
    EFFECTS = Cs((V'EFFECT' + (V'NEWLINE' - V'CURLY_CLOSE'))^0),

    EFFECT  = V'COMMENT' + V'SPECIAL' + V'CONSTANT' + V'OPERATOR' + V'CURLY'
            + V'SPACE' + (1 - S'\n{}'),

    -- Possible effects
    COMMENT  = (P'#' / '\\#') * ((P'#' / '\\#') + P(1) - '\n')^0,
    SPECIAL  = (GPR + SPR + P'mem' * (-alnum)) / special,
    CONSTANT = CONSTS / constant,
    OPERATOR = OP / operator_table,
    NEWLINE  = P'\n' * sp^0 / ([[\\]]..'\n'),
    SPACE    = P' ' / '\\ ',

    -- Indents curly blocks
    CURLY   = V'CURLY_OPEN' * V'CURLY_CLOSE'
            + V'CURLY_OPEN' * (V'COMMENT' * '\n')^-1
            * indent_begin * V'EFFECTS' * indent_end * V'CURLY_CLOSE',
    CURLY_OPEN  = (P'{' * sp^0 * P'\n'^-1) / ([[\{]] .. '\n'),
    CURLY_CLOSE = (P'\n'^-1 * sp^0 * P'}') / ('\n' .. [[\}]])
}

return effect
