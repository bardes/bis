local utils = require 'utils'

local registers = {
    length = 32,    -- Register length in bits
    n_gprs = 32,    -- Number of general purpose registers
    n_sprs = 16     -- Number of special purpose registers
}

-- Total ammount of registers in the architecture
registers.n_regs = registers.n_gprs + registers.n_sprs

-- Special Purpose Registers
registers.spr = {}
registers.spr.byMnemonic = {
    mar = {
        name = 'Memory Address Register',
        id = 0x20
    },

    mdr = {
        name = 'Memory Data Register',
        id = 0x21
    },

    pc = {
        name = 'Program Counter',
        id = 0x22
    },

    irh = {
        name = 'Instruction Register High',
        id = 0x23
    },

    irl = {
        name = 'Instruction Register Low',
        id = 0x24
    },

    sp = {
        name = 'Stack Pointer',
        id = 0x25
    },

    fp = {
        name = 'Frame Pointer',
        id = 0x26
    },

    slr = {
        name = 'Stack Limit Register',
        id = 0x27
    },

    fr = {
        name = 'Flag Register',
        id = 0x28
    },

    zr = {
        name = 'Zero Register',
        id = 0x29
    },

    im = {
        name = 'Interrupt Mask',
        id = 0x2A
    },

    cc = {
        name = 'Cycle Counter',
        id = 0x2B
    },

    ext3 = {
        name = 'Extra Register 3',
        id = 0x2C
    },

    ext2 = {
        name = 'Extra Register 2',
        id = 0x2D
    },

    ext1 = {
        name = 'Extra Register 1',
        id = 0x2E
    },

    ext0 = {
        name = 'Extra Register 0',
        id = 0x2F
    }
}

-- Creates a copy of the SPRs indexed by the IDs
registers.spr.byId = utils.reindex(registers.spr.byMnemonic, 'id', 'mnemonic')

-- Flag Register specifc info
registers.flag_register = {
    z = {
        name = 'Zero Flag',
        position = 0,
        description = [[
            This bit is set by the ULA when the result is zero, otherwise it is
            cleared.
        ]]
    },

    c = {
        name = 'Carry Flag',
        position = 1,
        description = [[
            Hold the carry out bit of arithmetic operations.
        ]]
    },

    n = {
        name = 'Negative Flag',
        position = 2,
        description = [[
            This bit is set to 1 when the result is negative. It is set by the
            32nd bit of the result, also known as "sign bit".
        ]]
    },

    o = {
        name = 'Overflow',
        position = 3,
        description = [[
            This bit is set when the two's complement representation of the
            result overflows.
        ]]
    },

    i = {
        name = 'Interrupt',
        position = 4,
        description = [[
            Enables interrupts when set to 1 and disables when set to 0.
        ]]
    },

    t = {
        name = 'Transfer',
        position = 5,
        description = [[
            This bit is used as a buffer for shift operations. It is used as a
            shift in value and stores the first bit shifted out.
        ]]
    },

    nan = {
        name = 'NaN Flag',
        position = 6,
        description = ''
    },

    e = {
        name = 'Endianness Bit',
        position = 7,
        description = [[
            Defines the endianess of memory accesses.

            1: Big Endian, 0: Little Endian
        ]]
    }
}

return registers
