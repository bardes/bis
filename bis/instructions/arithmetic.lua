local instructions = {
        add = {
            name = 'Addition',
            format = 'R3',

            opcode = 0x00,
            cycles = 1,

            description = 'Adds ry and rz, saving the result into rx.',
            effect = [[
                rx <- ry + rz;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = { 'z', 'c', 'n', 'o'},

            example = [[
                add r0, r1, r2 ; Writes r1 + r2 into r0
                add r0, r0, r1 ; Adds r1 into r0
                add r0, r0, r0 ; Doubles r0
            ]],
        },

        addc = {
            name = 'Addition With Carry',
            format = 'R3',

            opcode = 0x01,
            cycles = 1,

            description = [[
                Adds ry + rz + the carry bit saving the result into rx.
            ]],
            effect = [[
                rx <- ry + rz + fr:c;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = { 'z', 'c', 'n', 'o'},

            example = [[
                ; Adds r3,r2 into r1,r0 (High, Low)
                add  r0, r0, r2 ; First a regular add for the
                                ; low words sets the carry bit...
                addc r1, r1, r3 ; Then add the high words and the carry bit
            ]]
        },

        sub = {
            name = 'Subtraction',
            format = 'R3',

            opcode = 0x02,
            cycles = 1,

            description = [[
                Performs the subtraction ry - rz and saves the result into rx.
            ]],
            effect = [[
                rx <- ry - rz;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = { 'z', 'c', 'n', 'o'},

            example = [[
                sub r0, r1, r2 ; Writes r1 - r2 into r0
                sub r0, r0, r1 ; Subtracts r1 from r0
                sub r0, zr, r0 ; Sets r0 to it's 2's complement
            ]]
        },

        subc = {
            name = 'Subtraction With Carry',
            format = 'R3',

            opcode = 0x03,
            cycles = 1,

            description = [[
                Performs the subtraction ry - rz - sr:c and saves the result
                into rx.
            ]],
            effect = [[
                rx <- ry - rz - fr:c;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = { 'z', 'c', 'n', 'o'},

            example = [[
                ; Subtracts r3,r2 from r1,r0 (High, Low)
                sub  r0, r0, r2 ; First a regular sub for the
                                ; low word sets the carry bit...
                subc r1, r1, r3 ; Then subc the high word and the carry bit
            ]]
        },

        mulu = {
            name = 'Multiply Unsigned',
            format = 'R4',

            opcode = 0x04,
            cycles = 1,

            description = [[
                Multiplies rz and rw saving the result into rx, ry.

                Note that the zero flag is set only when rx, ry are zero.
            ]],
            effect = [[
                rx, ry <- rz * rw;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z'},

            example = [[
                addi r2, zr, 0x100
                addi r3, zr, 0xDEADBEEF
                mulu r0, r1, r2, r3 ; r0,r1 <- 0x000000DE,0xADBEEF00

                addi r2, zr, 0xFFFFFFFF ; (2^32 - 1)
                mulu r0, r1, r2, r2 ; r0,r1 <- 0xFFFFFFFE,0x00000001
            ]]
        },

        muls = {
            name = 'Multiply Signed',
            format = 'R4',

            opcode = 0x05,
            cycles = 1,

            description = [[
                Multiplies rz and rw saving the result into rx,ry.

                Note that the flags are set as if rx, ry were a 64 bit integer.
            ]],
            effect = [[
                rx, ry <- rz * rw;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z', 'n'},

            example = [[
                addi r2, zr, 0x100
                addi r3, zr, 0xDEADBEEF
                muls r0, r1, r2, r3 ; r0,r1 <- 0xFFFFFFDE,0xADBEEF00

                addi r2, zr, 0xFFFFFFFF ; (-1)
                muls r0, r1, r2, r2 ; r0,r1 <- 0x00000000,0x00000001
            ]]
        },

        divu = {
            name = 'Divide Unsigned',
            format = 'R3',

            opcode = 0x06,
            cycles = 1,

            description = [[
                Performs the unsigned integer division ry / rz, truncating the
                result towards zero and saving it into rx.
            ]],
            effect = [[
                rx <- ry / rz;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z'},

            example = [[
                addi r1, zr, 13
                addi r2, zr, 5
                divu r0, r1, r2 ; r0 is set to 2
            ]]
        },

        divs = {
            name = 'Divide Signed',
            format = 'R3',

            opcode = 0x07,
            cycles = 1,

            description = [[
                Performs the signed division ry / rz saving the result into rx.

                Note that the result is truncated towards zero.
            ]],
            effect = [[
                rx <- ry / rz;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z', 'n'},

            example = [[
                addi r1, zr, 13
                addi r2, zr, 5
                divs r0, r1, r2 ; r0 is set to 2
                addi r2, zr, -5
                divs r0, r1, r2 ; r0 is set to -2
            ]]
        },

        modu = {
            name = 'Unsigned Modulo',
            format = 'R3',

            opcode = 0x08,
            cycles = 1,

            description = [[
                Performs the unsigned modulo ry mod rz saving the result into
                rx.

                Note that for unsigned numbers the modulo and remainder are
                identical.
            ]],
            effect = [[
                rx <- ry mod rz;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z'},

            example = [[
                addi r1, zr, 13
                addi r2, zr, 10
                modu r0, r1, r2 ; r0 is set to 3
            ]]
        },

        mods = {
            name = 'Signed Modulo',
            format = 'R3',

            opcode = 0x09,
            cycles = 1,

            description = [[
                Performs the signed modulo ry mod rz saving the result into rx.

                Note that for signed numbers the mod operation will keep the
                same sign as rz but the value may differ depending on the value
                of ry.
            ]],
            effect = [[
                rx <- ry mod rz;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z', 'n'},

            example = [[
                addi r1, zr, 13
                addi r2, zr, -5
                mods r0, r1, r2 ; r0 is set to -2

                addi r1, zr, -13
                addi r2, zr, 5
                mods r0, r1, r2 ; r0 is set to 2

                addi r1, zr, -13
                addi r2, zr, -5
                mods r0, r1, r2 ; r0 is set to -3
            ]]
        },

        rems = {
            name = 'Signed Remainder',
            format = 'R3',

            opcode = 0x0A,
            cycles = 1,

            description = [[
                Takes the signed remainder of ry / rz saving the result into rx.

                This is (usually) the operation associated with the C operator
                '%'. Note that the result has always the same sign as ry and the
                same absolute value regardless of the sign of ry or rz.
            ]],
            effect = [[
                rx <- ry rem rz;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z', 'n'},

            example = [[
                addi r1, zr, 13
                addi r2, zr, -5
                rems r0, r1, r2 ; r0 is set to 3

                addi r1, zr, -13
                addi r2, zr, 5
                rems r0, r1, r2 ; r0 is set to -3

                addi r1, zr, -13
                addi r2, zr, -5
                rems r0, r1, r2 ; r0 is set to -3
            ]]
        },

        addi = {
            name = 'Addition with Immediate',
            format = 'I3',

            opcode = 0x10,
            cycles = 1,

            description = 'Adds ry + rz and saves the result into rx.',
            effect = [[
                rx <- ry + immediate;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = { 'z', 'c', 'n', 'o'},

            example = [[
                addi r0, r0, 1 ; Inccrements r0
                addi r0, r0, -1 ; Decrements r0
                addi r0, zr, 42 ; Sets r0 to 42
                addi r1, r0, 58 ; r1 <- 100
            ]]
        },

        subi = {
            name = 'Subtraction from Immediate',
            format = 'I3',

            opcode = 0x12,
            cycles = 1,

            description = [[
                Performs the subtraction immediate - ry and saves the result
                into rx.

                Hint: If you wish to subtract an immediate value from a register
                you can use the addi instruction with a negative immediate
                value.
            ]],
            effect = [[
                rx <- immediate - ry;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = { 'z', 'c', 'n', 'o'},

            example = [[
                addi r1, zr, 7
                subi r0, r1, 10 ; r0 <- 3
            ]]
        },

        mului = {
            name = 'Multiply Unsigned with Immediate',
            format = 'I4',

            opcode = 0x14,
            cycles = 1,

            description = [[
                Multiplies ry by immediate and saves the result into
                rx, ry.
            ]],
            effect = [[
                rx, ry <- rz * immediate;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z'},

            example = [[
                addi r2, zr, 0x12345678
                mului r0, r1, r2, 0x100; r0:r1 <- 0x00000012:0x4567800
                mului zr, r0, r2, 0x100; r0 <- 0x4567800

                addi r2, zr, 0xFFFFFFFF
                mului r0, r1, r2, 0xFFFFFFFF ; r0:r1 <- 0xfffffffe:0x00000001
            ]]
        },

        mulsi = {
            name = 'Multiply Signed with Immediate',
            format = 'I4',

            opcode = 0x15,
            cycles = 1,

            description = [[
                Multiplies rz by the immediate and saves the result into
                rx, ry.
            ]],
            effect = [[
                rx, ry <- rz * immediate
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z', 'n'},

            example = [[
                addi r2, zr, 0x12345678
                mulsi r0, r1, r2, 0x100 ; r0:r1 <- 0x00000012:0x4567800
                mulsi zr, r0, r2, 0x100 ; r0 <- 0x4567800

                addi r2, zr, -1 ;
                mulsi r0, r1, r2, -1 ; r0:r1 <- 0x00000000:0x00000001

                ; -1 == 0xFFFFFFFF == -0x00000001
                addi r2, zr, 0xFFFFFFFF
                mulsi r0, r1, r2, 0xFFFFFFFF ; r0:r1 <- 0x00000000:0x00000001
            ]]
        },

        divui = {
            name = 'Divide Unsigned with Immediate',
            format = 'I3',

            opcode = 0x16,
            cycles = 1,

            description = [[
                Performs the unsigned division ry / immediate, truncating the
                result towards zero and saving the result into rx.
            ]],
            effect = [[
                rx <- ry / immediate;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z'},

            example = [[
                addi r1, zr, 13
                divui r0, r1, 5 ; r0 is set to 2
            ]]
        },

        divsi = {
            name = 'Divide Signed with Immediate',
            format = 'I3',

            opcode = 0x17,
            cycles = 1,

            description = [[
                Performs the signed division ry / immediate, truncating the
                result towards zero and saving the result into rx.
            ]],
            effect = [[
                rx <- ry / immediate;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z', 'n'},

            example = [[
                addi r1, zr, 13
                divsi r0, r1, 5 ; r0 is set to 2
                divsi r0, r1, -5 ; r0 is set to -2
            ]]
        },

        modui = {
            name = 'Unsigned Modulo with Immediate',
            format = 'I3',

            opcode = 0x18,
            cycles = 1,

            description = [[
                Performs the unsigned modulo of ry with the immediate saving the
                result into rx.
            ]],
            effect = [[
                rx <- ry mod immediate;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z'},

            example = [[
                addi r1, zr, 13
                modui r0, r1, 10 ; r0 is set to 3
            ]]
        },

        modsi = {
            name = 'Signed Modulo with Immediate',
            format = 'I3',

            opcode = 0x19,
            cycles = 1,

            description = [[
                Performs the signed modulo of ry with the immediate saving the
                result into rx.

                For more information about the sign of the
                result see the 'mods' instruction.
            ]],
            effect = [[
                rx <- ry mod immediate;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z', 'n'},

            example = [[
                addi r1, zr, 13
                mods r0, r1, -5 ; r0 is set to -2

                addi r1, zr, -13
                mods r0, r1, 5 ; r0 is set to 2
                mods r0, r1, -5 ; r0 is set to -3
            ]]
        },

        remsi = {
            name = 'Signed Remainder with Immediate',
            format = 'I3',

            opcode = 0x1A,
            cycles = 1,

            description = [[
                Takes the signed remainder of ry / immediate saving the result
                into rx.

                For more information about the sign of the result see
                the 'rems' instruction.
            ]],
            effect = [[
                rx <- ry rem immediate;
                pc <- pc + size;
                cc <- cc + cycles;
            ]],
            status_register = {'z', 'n'},

            example = [[
                addi r1, zr, 13
                rems r0, r1, -5 ; r0 is set to 3

                addi r1, zr, -13
                rems r0, r1, 5 ; r0 is set to -3
                rems r0, r1, -5 ; r0 is set to -3
            ]]
        }
}

for i, _ in pairs(instructions) do
    instructions[i].class = 'arithmetic'
end

return instructions
