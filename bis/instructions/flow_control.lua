local instructions = {
    jmp = {
        name = 'Absolute Jump',
        format = 'J',

        opcode = 0x68,
        cycles = 1,

        description = [[
            If the condition is true pc is set to the given address, otherwise
            pc is incremented to the next instruction.
        ]],
        effect = [[
            if (condition) {
                pc <- address;
            } else {
                pc <- pc + size;
            }
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            sub zr, r0, r1 ; Compares r0 and r1
            jmp ugr, handle_greater ; Jumps if r0 > r1
            jmp ule, handle_lesser ; Jumps if r0 < r1
        ]]
    },

    call = {
        name = 'Absolute Call',
        format = 'J',

        opcode = 0x69,
        cycles = 1,

        description = [[
            If the condition is true the return address is pushed into the stack
            and pc is set to the given address, otherwise pc is incremented to
            the next instruction.
        ]],
        effect = [[
            if (condition) {
                if (fr:e == 1) { # Big Endian
                    mem[sp - 0] <- (pc + size).ll;
                    mem[sp - 1] <- (pc + size).l;
                    mem[sp - 2] <- (pc + size).h;
                    mem[sp - 3] <- (pc + size).hh;
                } else { # Little Endian
                    mem[sp - 3] <- (pc + size).ll;
                    mem[sp - 2] <- (pc + size).l;
                    mem[sp - 1] <- (pc + size).h;
                    mem[sp - 0] <- (pc + size).hh;
                }
                sp <- sp - 4;
                pc <- address;
            } else {
                pc <- pc + size;
            }
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            sub zr, r0, r1 ; Compares r0 and r1
            call ugr, handle_greater ; Calls if r0 > r1
            call ule, handle_lesser  ; Calls if r0 < r1
            call unc, handle_equal   ; Calls if r0 == r1
        ]]
    },

    rjmp = {
        name = 'Relative Jump',
        format = 'RJ',

        opcode = 0x6A,
        cycles = 1,

        description = [[
            If the condition is true (offset * 4) is added to pc, otherwise pc
            is incremented to the next instruction.
        ]],
        effect = [[
            if (condition) {
                pc <- pc + (offset << 2);
            } else {
                pc <- pc + size;
            }
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            sub zr, r0, r1 ; Compares r0 and r1

            rjmp ueg, 2 ; If r0 >= r1 jump two words
            swap r0, r1 ; else swap r0 and r1

            sub r0, r0, r1 ; r0 <- r0 - r1
        ]]
    },

    rcall = {
        name = 'Relative Call',
        format = 'RJ',

        opcode = 0x6B,
        cycles = 1,

        description = [[
            If the condition is true the return address is pushed into the stack
            and 'offset' words are added to pc, otherwise pc is incremented to
            the next instruction.
        ]],
        effect = [[
            if (condition) {
                if (fr:e == 1) { # Big Endian
                    mem[sp - 0] <- (pc + size).ll;
                    mem[sp - 1] <- (pc + size).l;
                    mem[sp - 2] <- (pc + size).h;
                    mem[sp - 3] <- (pc + size).hh;
                } else { # Little Endian
                    mem[sp - 3] <- (pc + size).ll;
                    mem[sp - 2] <- (pc + size).l;
                    mem[sp - 1] <- (pc + size).h;
                    mem[sp - 0] <- (pc + size).hh;
                }
                sp <- sp - 4;
                pc <- pc + (offset << 2);
            } else {
                pc <- pc + size;
            }
            cc <- cc + 1;
        ]],
        status_register = {},

        example = [[
            add r0, r1, r2 ; Dummy procedure
            sub r1, r1, r2
            divs r0, r1, r0
            ret

            rcall unc, -4 ; Calls the address 4 words before this
        ]]
    },

    ijmp = {
        name = 'Indirect Jump',
        format = 'IJ',

        opcode = 0x6C,
        cycles = 1,

        description = [[
            Jumps to the address pointed by ry + rz. This is useful to implement
            position independent code, and jumps to positions that are only
            known at run time.
            Note that the sum ry + rz is done just like the add instruction, but
            it won't set any flags.
        ]],
        effect = [[
            if (condition) {
                pc <- ry + rz;
            } else {
                pc <- pc + size;
            }
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            load32i r0, 0x00000008 ; Loads a target address
            ijmp zr, r0; And jumps to it
        ]]
    },

    icall = {
        name = 'Indirect Call',
        format = 'IJ',

        opcode = 0x6D,
        cycles = 1,

        description = [[
            If the condition is true the return address is pushed into the stack
            and pc is set to the sum of ry and rz, otherwise pc is incremented
            to the next instruction.
        ]],
        effect = [[
            if (condition) {
                if (fr:e == 1) { # Big Endian
                    mem[sp - 0] <- (pc + size).ll;
                    mem[sp - 1] <- (pc + size).l;
                    mem[sp - 2] <- (pc + size).h;
                    mem[sp - 3] <- (pc + size).hh;
                } else { # Little Endian
                    mem[sp - 3] <- (pc + size).ll;
                    mem[sp - 2] <- (pc + size).l;
                    mem[sp - 1] <- (pc + size).h;
                    mem[sp - 0] <- (pc + size).hh;
                }
                sp <- sp - 4;
                pc <- ry + rz;
            } else {
                pc <- pc + size;
            }
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            addi r0, zr, 4
            icall sp, r0 ; Calls the address at the top of the stack
        ]]
    },

    ret = {
        name = 'Return From Call',
        format = 'R0',

        opcode = 0x6E,
        cycles = 1,

        description = [[
            Returns from a call by poping the top of the satack into pc.
        ]],
        effect = [[
            if (fr:e == 1) { # Big Endian
                pc.hh <- mem[sp + 1];
                pc.h  <- mem[sp + 2];
                pc.l  <- mem[sp + 3];
                pc.ll <- mem[sp + 4];
            } else { # Little Endian
                pc.ll <- mem[sp + 1];
                pc.l  <- mem[sp + 2];
                pc.h  <- mem[sp + 3];
                pc.hh <- mem[sp + 4];
            }
            sp <- sp + 4;
        ]],
        status_register = {},

        example = [[
            push r10
            push r11

            add r10, r10, r10
            add r11, r11, r11
            add r0, r10, r11

            pop r11
            pop r10
            ret ; Returns from procedure
        ]]
    }
}

for i, _ in pairs(instructions) do
    instructions[i].class = 'flow_control'
end

return instructions
