local instructions = {
    movbit = {
        name = 'Move Bit',
        format = 'M',

        opcode = 0x78,
        cycles = 1,

        description = [[
            Moves the k-th bit of ry into the j-th bit of rx.
        ]],
        effect = [[
            rx:j <- ry:k;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z'},

        example = [[
            addi r0, zr, 0x00
            addi r1, zr, 0x18
            movbit r0, r1, 7, 3 ; r0:7 <- r1:3
        ]]
    },

    push = {
        name = 'Push',
        format = 'R1',

        opcode = 0x79,
        cycles = 1,

        description = [[
            Pushes the contents of rx into the stack.
        ]],
        effect = [[
            if (fr:e == 1) { # Big Endian
                mem[sp - 0] <- rx.ll;
                mem[sp - 1] <- rx.l;
                mem[sp - 2] <- rx.h;
                mem[sp - 3] <- rx.hh;
            } else { # Little Endian
                mem[sp - 0] <- rx.hh;
                mem[sp - 1] <- rx.h;
                mem[sp - 2] <- rx.l;
                mem[sp - 3] <- rx.ll;
            }
            sp <- sp - 4;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            addi r0, zr, 0xDEADBEEF
            push r0 ; Pushes 0xDEADBEEF into the stack
        ]]
    },

    pop = {
        name = 'Pop',
        format = 'R1',

        opcode = 0x7A,
        cycles = 1,

        description = [[
            Pops the top of the stack into rx.
        ]],
        effect = [[
            if (fr:e == 1) { # Big Endian
                rx.hh <- mem[sp + 1];
                rx.h  <- mem[sp + 2];
                rx.l  <- mem[sp + 3];
                rx.ll <- mem[sp + 4];
            } else { # Little Endian
                rx.ll <- mem[sp + 1];
                rx.l  <- mem[sp + 2];
                rx.h  <- mem[sp + 3];
                rx.hh <- mem[sp + 4];
            }
            sp <- sp + 4;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            addi r0, zr, 0xDEADBEEF
            push r0 ; Pushes 0xDEADBEEF into the stack
            pop r1 ; Pops the top of the stack into r1
        ]]
    },

    swap = {
        name = 'Swap',
        format = 'R2',

        opcode = 0x7B,
        cycles = 1,

        description = [[
            Swaps the content of rx and ry.
        ]],
        effect = [[
            rx <- ry;
            ry <- rx;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            addi r0, zr, 0x00000000
            addi r1, zr, 0x11111111

            swap r0, r1 ; r0 <- 0x11111111
                        ; r1 <- 0x00000000
        ]]
    },

    ['break'] = {
        name = 'Break',
        format = 'R0',

        opcode = 0x7C,
        cycles = 1,

        description = [[
            On the emulator this instruction prints the internal state of the
            machine and halts execution until the user orders it to resume.
        ]],
        effect = [[
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            add r0, r1, r2 ; Writes r1 + r2 into r0
            break          ; Breaks after adding
            add r0, r0, r0 ; Resumes execution after break
        ]],
    },

    syscall = {
        name = 'System Call',
        format = 'R1',

        opcode = 0x7D,
        cycles = 1,

        description = [[
            Executes the syscall specified by rx.
        ]],
        effect = [[
            # Dependes on the syscall code.
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'?'},

        example = [[
            addi r0, zr, 1 ; Load r0 with the syscall code.
            syscall r0
        ]]
    },

    nop = {
        name = 'No Operation',
        format = 'R0',

        opcode = 0x7F,
        cycles = 1,

        description = [[
            This instruction has no effect other than advancing the program
            counter to the next instruction.
        ]],
        effect = [[
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},

        example = [[
            nop
            nop ; Who's there?
        ]],
    }
}

for i, _ in pairs(instructions) do
    instructions[i].class = 'misc'
end

return instructions
