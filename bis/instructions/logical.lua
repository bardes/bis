local instructions = {
    ['not'] = {
        name = 'Bitwise Not',
        format = 'R2',

        opcode = 0x40,
        cycles = 1,
        description = [[
            Stores the negation of ry into rx.
        ]],

        effect = [[
            rx <- not ry;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z'},
        example = [[
            addi r0, zr, 0x0000FFFF

            not r0, r0 ; r0 is set to 0xFFFF0000
            not r0, r0 ; r0 is set back to 0x0000FFFF
        ]]
    },

    ['and'] = {
        name = 'Bitwise And',
        format = 'R3',

        opcode = 0x41,
        cycles = 1,
        description = [[
            Stores the bitwise 'and' of ry and rz into rx.
        ]],

        effect = [[
            rx <- ry and rz;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z'},
        example = [[
            addi r1, zr, 0x00FF00FF
            addi r2, zr, 0x0000FFFF

            and r0, r1, r2 ; r0 is set to 0x000000FF
        ]]
    },

    ['or'] = {
        name = 'Bitwise Or',
        format = 'R3',

        opcode = 0x42,
        cycles = 1,
        description = [[
            Stores the bitwise 'or' of ry and rz into rx.
        ]],

        effect = [[
            rx <- ry or rz;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z'},
        example = [[
            addi r1, zr, 0x00FF00FF
            addi r2, zr, 0x0000FFFF

            or r0, r1, r2 ; r0 is set to 0x00FFFFFF
        ]]
    },

    xor = {
        name = 'Bitwise Exclusive Or',
        format = 'R3',

        opcode = 0x43,
        cycles = 1,
        description = [[
            Stores the bitwise exclusive or of ry and rz into rx.
        ]],

        effect = [[
            rx <- ry xor rz;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z'},
        example = [[
            addi r1, zr, 0x00FF00FF
            addi r2, zr, 0x0000FFFF

            xor r0, r1, r2 ; r0 is set to 0x00FFFF00
        ]]
    },

    shiftl = {
        name = 'Shifht Left',
        format = 'R3',

        opcode = 0x44,
        cycles = 1,
        description = [[
            Stores ry shifted left by rz bits into rx. The bits shifted in are
            set to the same value as the transfer bit. After the operation the
            tansfer bit will be set to the value of the first bit shifted out.
            Note that rz is taken mod 32.
        ]],

        effect = [[
            rx <- ry << (rz mod 32);
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z', 't'},
        example = [[
            andi fr, 0xFFFFFFDF ; Sets the transfer bit to zero
            addi r1, zr, 0x12345678
            addi r2, zr, 8

            shiftl r0, r1, r2 ; r0 is set to 0x34567800
        ]]
    },

    shiftr = {
        name = 'Shifht Right',
        format = 'R3',

        opcode = 0x45,
        cycles = 1,
        description = [[
            Stores ry shifted right by rz bits into rx. The bits shifted in are
            set to the same value as the transfer bit. After the operation the
            tansfer bit will be set to the value of the first bit shifted out.
            Note that rz is taken mod 32.
        ]],

        effect = [[
            rx <- ry >> (rz mod 32);
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z', 't'},
        example = [[
            andi fr, 0xFFFFFFDF ; Sets the transfer bit to zero
            addi r1, zr, 0x12345678
            addi r2, zr, 8

            shiftr r0, r1, r2 ; r0 is set to 0x00123456
        ]]
    },

    rotl = {
        name = 'Rotate Left',
        format = 'R3',

        opcode = 0x46,
        cycles = 1,
        description = [[
            Stores ry rotated left by rz bits into rx.
            Note that rz is taken mod 32.
        ]],

        effect = [[
            rx <- ry rotl (rz mod 32);
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = [[
            addi r1, zr, 0x12345678
            addi r2, zr, 8

            rotl r0, r1, r2 ; r0 is set to 0x34567812
        ]]
    },

    rotr = {
        name = 'Rotate Right',
        format = 'R3',

        opcode = 0x47,
        cycles = 1,
        description = [[
            Stores ry rotated right by rz bits into rx.
            Note that rz is taken mod 32.
        ]],

        effect = [[
            rx <- ry rotr (rz mod 32);
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = [[
            addi r1, zr, 0x12345678
            addi r2, zr, 8

            rotr r0, r1, r2 ; r0 is set to 0x78123456
        ]]
    },

    andi = {
        name = 'Bitwise And with Immediate',
        format = 'I3',

        opcode = 0x51,
        cycles = 1,
        description = [[
            Sets r0 to the bitwise 'and' of ry and the immediate value.
        ]],

        effect = [[
            rx <- ry and immediate;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z'},
        example = [[
            andi r0, r0, 0 ; Sets r0 to 0
            andi r0, fr, 0x00000020 ; Gets the transfer bit
        ]]
    },

    ori = {
        name = 'Bitwise Or with Immediate',
        format = 'I3',

        opcode = 0x52,
        cycles = 1,
        description = [[
            Sets r0 to the bitwise 'or' of ry and the immediate value.
        ]],

        effect = [[
            rx <- ry or immediate;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z'},
        example = [[
            addi r1, zr, 0xFFFF0000
            ori  r0, r1, 0x00FF00FF ; r0 is set to 0xFFFF00FF
        ]]
    },

    xori = {
        name = 'Bitwise Exclusive Or with Immediate',
        format = 'I3',

        opcode = 0x53,
        cycles = 1,
        description = [[
            Sets r0 to the bitwise 'xor' of ry and the immediate value.
        ]],

        effect = [[
            rx <- ry xor immediate;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z'},
        example = [[
            addi r1, zr, 0xFFFF0000
            xori r0, r1, 0x00FF00FF ; r0 is set to 0xFF0000FF
        ]]
    },

    shiftli = {
        name = 'Shift Left by Immediate',
        format = 'I3',

        opcode = 0x54,
        cycles = 1,
        description = [[
            Stores ry shifted left by 'immediate' bits into rx. The bits
            shifted in are set to the same value as the transfer bit. After the
            operation the tansfer bit will be set to the value of the first bit
            shifted out.
            Note that the immediate value is taken mod 32.
        ]],

        effect = [[
            rx <- ry << (immediate mod 32);
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z', 't'},
        example = [[
            addi r1, zr, 0x12345678

            andi fr, 0xFFFFFFDF ; Sets the transfer bit to zero
            shiftli r0, r1, 8 ; r0 is set to 0x34567800

            ori fr, 0x00000020 ; Sets the transfer bit to one
            shiftli r0, r1, 8 ; r0 is set to 0x345678FF
        ]]
    },

    shiftri = {
        name = 'Shift Right by Immediate',
        format = 'I3',

        opcode = 0x55,
        cycles = 1,
        description = [[
            Stores ry shifted right by 'immediate' bits into rx. The bits
            shifted in are set to the same value as the transfer bit. After the
            operation the tansfer bit will be set to the value of the first bit
            shifted out.
            Note that the immediate value is taken mod 32.
        ]],

        effect = [[
            rx <- ry >> (immediate mod 32);
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {'z', 't'},
        example = [[
            addi r1, zr, 0x12345678

            andi fr, 0xFFFFFFDF ; Sets the transfer bit to zero
            shiftri r0, r1, 8 ; r0 is set to 0x00123456

            ori fr, 0x00000020 ; Sets the transfer bit to one
            shiftri r0, r1, 8 ; r0 is set to 0xFF123456
        ]]
    },

    rotli = {
        name = 'Rotate Left by Immediate',
        format = 'I3',

        opcode = 0x56,
        cycles = 1,
        description = [[
            Stores ry rotated left by 'immediate' bits into rx.
            Note that the immediate value is taken mod 32.
        ]],

        effect = [[
            rx <- ry rotl (immediate mod 32);
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = [[
            addi r1, zr, 0x12345678
            rotli r0, r1, 8 ; r0 is set to 0x34567812
        ]]
    },

    rotri = {
        name = 'Rotate Right by Immediate',
        format = 'I3',

        opcode = 0x57,
        cycles = 1,
        description = [[
            Stores ry rotated right by 'immediate' bits into rx.
            Note that the immediate value is taken mod 32.
        ]],

        effect = [[
            rx <- ry rotr (immediate mod 32);
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = [[
            addi r1, zr, 0x12345678
            rotri r0, r1, 8 ; r0 is set to 0x78123456
        ]]
    }
}

for i, _ in pairs(instructions) do
    instructions[i].class = 'logical'
end

return instructions
