local instructions = {
    load64 = {
        name = '64-bit Load',
        format = 'R3',

        opcode = 0x60,
        cycles = 1,
        description = [[
            Loads rx and ry with the contents of the memory address pointed by
            rz.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                rx.hh <- mem[rz + 0];
                rx.h  <- mem[rz + 1];
                rx.l  <- mem[rz + 2];
                rx.ll <- mem[rz + 3];
                ry.hh <- mem[rz + 4];
                ry.h  <- mem[rz + 5];
                ry.l  <- mem[rz + 6];
                ry.ll <- mem[rz + 7];
            } else { # Little Endian
                ry.ll <- mem[rz + 0];
                ry.l  <- mem[rz + 1];
                ry.h  <- mem[rz + 2];
                ry.hh <- mem[rz + 3];
                rx.ll <- mem[rz + 4];
                rx.l  <- mem[rz + 5];
                rx.h  <- mem[rz + 6];
                rx.hh <- mem[rz + 7];
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    store64 = {
        name = '64-bit Store',
        format = 'R3',

        opcode = 0x61,
        cycles = 1,
        description = [[
            Stores ry and rz into the memory address pointed by rx.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                mem[rx + 0] <- ry.hh;
                mem[rx + 1] <- ry.h;
                mem[rx + 2] <- ry.l;
                mem[rx + 3] <- ry.ll;
                mem[rx + 4] <- rz.hh;
                mem[rx + 5] <- rz.h;
                mem[rx + 6] <- rz.l;
                mem[rx + 7] <- rz.ll;
            } else { # Little Endian
                mem[rx + 0] <- rz.ll;
                mem[rx + 1] <- rz.l;
                mem[rx + 2] <- rz.h;
                mem[rx + 3] <- rz.hh;
                mem[rx + 4] <- ry.ll;
                mem[rx + 5] <- ry.l;
                mem[rx + 6] <- ry.h;
                mem[rx + 7] <- ry.hh;
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    load32 = {
        name = '32-bit Load',
        format = 'R2',

        opcode = 0x62,
        cycles = 1,
        description = [[
            Loads rx with the contents of the memory address pointed by ry.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                rx.hh <- mem[ry + 0];
                rx.h  <- mem[ry + 1];
                rx.l  <- mem[ry + 2];
                rx.ll <- mem[ry + 3];
            } else { # Little Endian
                rx.ll <- mem[ry + 0];
                rx.l  <- mem[ry + 1];
                rx.h  <- mem[ry + 2];
                rx.hh <- mem[ry + 3];
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    store32 = {
        name = '32-bit Store',
        format = 'R2',

        opcode = 0x63,
        cycles = 1,
        description = [[
            Stores ry into the memory address pointed by rx.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                mem[rx + 0] <- ry.hh;
                mem[rx + 1] <- ry.h;
                mem[rx + 2] <- ry.l;
                mem[rx + 3] <- ry.ll;
            } else { # Little Endian
                mem[rx + 0] <- ry.ll;
                mem[rx + 1] <- ry.l;
                mem[rx + 2] <- ry.h;
                mem[rx + 3] <- ry.hh;
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    load16 = {
        name = '16-bit Load',
        format = 'R2',

        opcode = 0x64,
        cycles = 1,
        description = [[
            Loads the least significant half of rx with the contents of the
            memory address pointed by ry.
            Note that only the least significant half of rx is modified.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                rx.l  <- mem[ry + 0];
                rx.ll <- mem[ry + 1];
            } else { # Little Endian
                rx.ll <- mem[ry + 0];
                rx.l  <- mem[ry + 1];
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    store16 = {
        name = '16-bit Store',
        format = 'R2',

        opcode = 0x65,
        cycles = 1,
        description = [[
            Stores the lower half from ry into the memory address held by rx.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                mem[rx + 0] <- ry.l;
                mem[rx + 1] <- ry.ll;
            } else { # Little Endian
                mem[rx + 0] <- ry.ll;
                mem[rx + 1] <- ry.l;
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    load8 = {
        name = '8-bit Load',
        format = 'R2',

        opcode = 0x66,
        cycles = 1,
        description = [[
            Loads the least significant byte of rx with the contents of the
            memory address pointed by ry.
            Note that only the least significant byte of rx is modified.
        ]],

        effect = [[
            rx.ll <- mem[ry];
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    store8 = {
        name = '8-bit Store',
        format = 'R2',

        opcode = 0x67,
        cycles = 1,
        description = [[
            Stores the least significant byte from ry into the memory address
            held in rx.
        ]],

        effect = [[
            mem[rx] <- ry.ll;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    loadi64 = {
        name = '64-bit Load with Immediate Address',
        format = 'I3',

        opcode = 0x70,
        cycles = 1,
        description = [[
            Loads rx and ry with the contents of the memory address pointed by
            the immediate.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                rx.hh <- mem[immediate + 0];
                rx.h  <- mem[immediate + 1];
                rx.l  <- mem[immediate + 2];
                rx.ll <- mem[immediate + 3];
                ry.hh <- mem[immediate + 4];
                ry.h  <- mem[immediate + 5];
                ry.l  <- mem[immediate + 6];
                ry.ll <- mem[immediate + 7];
            } else { # Little Endian
                ry.ll <- mem[immediate + 0];
                ry.l  <- mem[immediate + 1];
                ry.h  <- mem[immediate + 2];
                ry.hh <- mem[immediate + 3];
                rx.ll <- mem[immediate + 4];
                rx.l  <- mem[immediate + 5];
                rx.h  <- mem[immediate + 6];
                rx.hh <- mem[immediate + 7];
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    storei64 = {
        name = '64-bit Store with Immediate Address',
        format = 'I3',

        opcode = 0x71,
        cycles = 1,
        description = [[
            Stores rx and ry into the memory address pointed by the immediate.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                mem[immediate + 0] <- rx.hh;
                mem[immediate + 1] <- rx.h;
                mem[immediate + 2] <- rx.l;
                mem[immediate + 3] <- rx.ll;
                mem[immediate + 4] <- ry.hh;
                mem[immediate + 5] <- ry.h;
                mem[immediate + 6] <- ry.l;
                mem[immediate + 7] <- ry.ll;
            } else { # Little Endian
                mem[immediate + 0] <- ry.ll;
                mem[immediate + 1] <- ry.l;
                mem[immediate + 2] <- ry.h;
                mem[immediate + 3] <- ry.hh;
                mem[immediate + 4] <- rx.ll;
                mem[immediate + 5] <- rx.l;
                mem[immediate + 6] <- rx.h;
                mem[immediate + 7] <- rx.hh;
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    loadi32 = {
        name = '32-bit Load with Immediate Address',
        format = 'I2',

        opcode = 0x72,
        cycles = 1,
        description = [[
            Loads rx with the contents of the memory address pointed by the immediate.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                rx.hh <- mem[immediate + 0];
                rx.h  <- mem[immediate + 1];
                rx.l  <- mem[immediate + 2];
                rx.ll <- mem[immediate + 3];
            } else { # Little Endian
                rx.ll <- mem[immediate + 0];
                rx.l  <- mem[immediate + 1];
                rx.h  <- mem[immediate + 2];
                rx.hh <- mem[immediate + 3];
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    storei32 = {
        name = '32-bit Store with Immediate Address',
        format = 'I2',

        opcode = 0x73,
        cycles = 1,
        description = [[
            Stores rx into the memory address pointed by the immediate.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                mem[immediate + 0] <- rx.hh;
                mem[immediate + 1] <- rx.h;
                mem[immediate + 2] <- rx.l;
                mem[immediate + 3] <- rx.ll;
            } else { # Little Endian
                mem[immediate + 0] <- rx.ll;
                mem[immediate + 1] <- rx.l;
                mem[immediate + 2] <- rx.h;
                mem[immediate + 3] <- rx.hh;
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    loadi16 = {
        name = '16-bit Load with Immediate Address',
        format = 'I2',

        opcode = 0x74,
        cycles = 1,
        description = [[
            Loads the least significant half of rx with the contents of the
            memory address pointed by the immediate.
            Note that only the least significant half of rx is modified.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                rx.l  <- mem[immediate + 0];
                rx.ll <- mem[immediate + 1];
            } else { # Little Endian
                rx.ll <- mem[immediate + 0];
                rx.l  <- mem[immediate + 1];
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    storei16 = {
        name = '16-bit Store with Immediate Address',
        format = 'I2',

        opcode = 0x75,
        cycles = 1,
        description = [[
            Stores the lower half of rx into the memory address pointed by the
            immediate.
        ]],

        effect = [[
            if (fr:e == 1) { # Big Endian
                mem[immediate + 0] <- rx.l;
                mem[immediate + 1] <- rx.ll;
            } else { # Little Endian
                mem[immediate + 0] <- rx.ll;
                mem[immediate + 1] <- rx.l;
            }
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    loadi8 = {
        name = '8-bit Load with Immediate Address',
        format = 'I2',

        opcode = 0x76,
        cycles = 1,
        description = [[
            Loads the least significant byte of rx with the contents of the
            memory address pointed by the immediate.
            Note that only the least significant byte of rx is modified.
        ]],

        effect = [[
            rx.ll <- mem[immediate];
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    },

    storei8 = {
        name = '8-bit Store with Immediate Address',
        format = 'I2',

        opcode = 0x77,
        cycles = 1,
        description = [[
            Stores the least significant byte from rx into the memory address
            pointed by the immediate.
        ]],

        effect = [[
            mem[immediate] <- rx.ll;
            pc <- pc + size;
            cc <- cc + cycles;
        ]],
        status_register = {},
        example = nil
    }
}

for i, _ in pairs(instructions) do
    instructions[i].class = 'load_store'
end

return instructions
