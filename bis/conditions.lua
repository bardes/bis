local utils = require 'utils'

local conditions = {}
conditions.byMnemonic = {
    unc = {
        name = 'Unconditional Jump',
        code = 0x00
    },

    ugr = {
        name = 'Unsigned Greater',
        code = 0x01
    },

    uel = {
        name = 'Unsigned Equal or Lesser',
        code = 0x11
    },

    ule = {
        name = 'Unsigned Lesser',
        code = 0x02
    },

    ueg = {
        name = 'Unsigned Equal or Greater',
        code = 0x12
    },

    sgr = {
        name = 'Signed Greater',
        code = 0x03
    },

    sel = {
        name = 'Signed Equal or Lesser',
        code = 0x13
    },

    sle = {
        name = 'Signed Lesser',
        code = 0x04
    },

    seg = {
        name = 'Signed Equal or Greater',
        code = 0x14
    },

    z = {
        name = 'Zero',
        code = 0x05
    },

    nz = {
        name = 'Not Zero',
        code = 0x15
    },

    c = {
        name = 'Carry',
        code = 0x06
    },

    nc = {
        name = 'Not Carry',
        code = 0x16
    },

    o = {
        name = 'Overflow',
        code = 0x07
    },

    no = {
        name = 'Not Overflow',
        code = 0x17
    },

    n = {
        name = 'Negative',
        code = 0x08
    },

    nn = {
        name = 'Not Negative',
        code = 0x18
    }
}

conditions.byCode = utils.reindex(conditions.byMnemonic, 'code', 'mnemonic')

return conditions
