\section{O Problema}
Dada a descrição da arquitetura no Capítulo~\ref{chapter:metodos} o desafio
agora tona-se implementá-la da maneira mais eficiente possível. Para testar o
\emph{design} da arquitetura de maneira mais rápida e eficaz faz sentido
começar seu desenvolvimento pelo montador e emulador, já que estes são
suficientes para testar a arquitetura e são mais rápidos de se implementar e
alterar durante a etapa de desenvolvimento.

Além disso, começar o desenvolvimento da arquitetura pelo simulador e emulador é
interessante, pois, a maior parte dos alunos passa mais tempo com essas
ferramentas do que com a implementação em \emph{hardware} ou \emph{FPGAs}. Além
do mais, a existência de um emulador é extremamente útil para o desenvolvimento
e teste do \emph{hardware} no futuro.

\section{Atividades Realizadas}
Para tornar o desenvolvimento o mais ágil possível, todas as informações sobre a
arquitetura foram definidas em apenas um lugar. Essas informações estão
registradas em tabelas \emph{Lua} no diretório \texttt{bis/} do
repositório\footnote{\url{gitlab.com/bardes/bis}}, onde o arquivo
\texttt{bis/arch.lua} é a ``tabela raiz'' contendo todas as informações sobre a
arquitetura.

Com todas as informações comuns organizadas em apenas um lugar é possível gerar
as ferramentas da arquitetura a partir delas, garantindo que elas sempre estarão
atualizadas e compatíveis. A tarefa de se gerar as ferramentas é feita pelo
\texttt{Makefile} presente na raiz do diretório.

As subseções a seguir explicam como cada ferramenta usa os dados da arquitetura,
justificando algumas das decisões de \emph{design} e destacando problemas e
soluções encontrados ao longo do desenvolvimento.

\subsection{Emulador}
Inicialmente, foi considerado usar apenas a linguagem \emph{Lua} para o
desenvolvimento tanto do emulador quanto do montador, porém testes preliminares
indicaram que o desempenho seria limitado em torno de 5~a~15~MIPS\footnote{Mega
Instruções Por Segundo}. O uso de \emph{Lua-JIT} melhorou o desempenho de alguns
testes para aproximadamente 35~MIPS, porém, esse ganho só foi observado para
``loops apertados e longos'', com códigos pequenos obtendo pouco ou nenhum
ganho.

Com essas considerações, a linguagem \emph{C} foi escolhida para o
desenvolvimento do emulador, justamente por sua capacidade de otimização e alto
desempenho. A escolha de uma linguagem diferente de \emph{Lua} porém requer
alguma forma de consultar os dados da arquitetura.

Apesar de \emph{Lua} oferecer uma excelente \emph{API} para a linguagem
\emph{C}, seu uso em tempo de execução levaria à mesma penalidade de tempo de
chamada observada com a linguagem \emph{Lua}, dessa forma, afim de se preservar
o desempenho do emulador, um script (\texttt{bis/gen.lua}) foi desenvolvido para
gerar trechos de código \emph{C} relevantes para o emulador. Esse script é
responsável por gerar uma série de definições e tabelas de consulta a partir dos
dados da arquitetura. O Código~\ref{codigo:gen} a seguir ilustra como exemplo
uma das funções responsáveis por gerar um arquivo de cabeçalho.

\begin{codigo}[%
    caption={Trecho do script bis/gen.lua},
    label={codigo:gen},
    language={[5.3]lua},
    breaklines=false
]
function files.conditions.gen()
  print(('/* %s */\n'):format(files.conditions.description))

  for mnemonic, condition in pairs(arch.conditions.byMnemonic) do
    print(("#define BIS_%s_COND ((bis_condition_t) 0x%XU) /* %s */")
    :format(mnemonic:upper(), condition.code, condition.name))
  end
end
\end{codigo}

No emulador esse arquivo é incluído (Código~\ref{codigo:inc_pulos}) e utilizado
para implementar o comportamento desejado (Código~\ref{codigo:verifica_cond}).

\begin{codigo}[%
    caption = {Trecho do cabeçalho emulator/bis.h},
    label = {codigo:inc_pulos},
    language=C,
    breaklines=false
]
#include "registers.gen"
#include "instructions.gen"
#include "conditions.gen"
\end{codigo}

\begin{codigo}[%
    caption={Função de verificação de condição de pulo (bis/instruction.c)},
    label={codigo:verifica_cond},
    language=C,
    breaklines=false
]
/* Verifies if the given condition is true given the state M. */
static int
verify_condition(const bis_machine_state_t *M, uint8_t condition)
{
    uint64_t result;

    /* Note that the first bit is ignored */
    switch(condition & 0xF) {

        /* Unconditional jump */
        case 0U: return 1;

        /* Here be crazy bit hacking */
        case BIS_UGR_COND: result = FR_C & ~FR_Z; break;
        case BIS_ULE_COND: result = ~(FR_C | FR_Z); break;
        case BIS_SGR_COND: result = ~((FR_O ^ FR_N) | FR_Z); break;
        case BIS_SLE_COND: result = ~FR_Z & (FR_O ^ FR_N); break;

        /* These are pretty straight forward... */
        case BIS_Z_COND: result = FR_Z; break;
        case BIS_C_COND: result = FR_C; break;
        case BIS_O_COND: result = FR_O; break;
        case BIS_N_COND: result = FR_N; break;

        default:
            ERROR_MSG("Unexpected condition code 0x%02hhX.",
            condition);
            return 0;
    }

    /* Clears all but the LSB of the result and flips it if we want
    * the opposite condition of what was tested. */
    return (int) ((result & 1LLU) ^ GET_BIT(condition, 4));
}
\end{codigo}

O Código~\ref{codigo:verifica_cond} também é útil para ilustrar como a
organização da tabela de condições de pulo permite uma ganho de
desempenho\footnote{Conforme mencionado na Seção~\ref{sec:controle_de_fluxo}},
já que apenas metade das condições precisa ser verificada, enquanto o \emph{ou
exclusivo} na última linha inverte o resultado caso o bit mais significativo da
condição seja 1.

\subsection{Montador}
Para o montador o desempenho de \emph{Lua} se mostrou mais que suficiente, além
disso, o pacote \emph{lpeg}~\cite{lpeg:2008} foi uma ferramenta
extremamente útil para a construção da gramática do montador a partir dos dados
arquiteturais definidos anteriormente. O Código~\ref{codigo:parser} mostra um
exemplo de uma regra gramatical do \emph{parser}, que é criada em tempo de
execução (do montador) consultando-se a os dados da arquitetura.

\begin{codigo}[%
    caption={Trecho do script bis/parser.lua},
    label={codigo:parser},
    language={[5.3]lua},
    breaklines=false
]
-- Special purpose registers
local SPR = T(errors.bad_register.id)
local sprs_byMnemonic = arch.registers.spr.byMnemonic
for _, mnemonic, reg in utils.sorted_pairs(sprs_byMnemonic) do
    SPR = mnemonic * (-alnum) * Cc(reg.id) + SPR
end
\end{codigo}

Essa técnica é útil, pois dessa forma não é necessário alterar o montador para se
adicionar, remover ou alterar registradores. A mesma técnica é usada para as
instruções e condições de pulo, o que permite bastante flexibilidade no
desenvolvimento e experimentação. Na Seção~\ref{sec:nova_inst} essa
flexibilidade sera ilustrada mais a fundo mostrando-se o processo de adição de
uma nova instrução.

\subsection{Documentação}
A organização de todos os dados arquiteturais em um só lugar ajuda a resolver um
dos problemas mais frequentes com as arquiteturas escolhidas em sala de aula: a
falta de documentação atualizada e de fácil compreensão.

A partir dos dados arquiteturais, e com o auxílio de alguns
\emph{templates}\footnote{Vale mencionar que foi utilizada a \emph{template
engine} ``\texttt{molde}'', desenvolvida pelo aluno (e colega) Gil Barbosa.\\
\url{https://luarocks.org/modules/gilzoide/molde}} é possível gerar
automaticamente documentos (txt, \LaTeX, html etc\ldots) contendo os dados mais
atuais sobre a arquitetura, e elimina o trabalho repetitivo de atualizar
múltiplas fontes.

Os Apêndices~\ref{appendix:tabela-de-instrucoes}~e~\ref{appendix:guia-de-referencia}
deste documento foram gerados automaticamente a partir de templates fornecidos
ao script \texttt{bis/gen.lua}. O Código~\ref{codigo:template} a seguir mostra o
template usado para gerar o Apêndice~\ref{appendix:guia-de-referencia}.

\lstinputlisting[%
    style=abnt_codestyle,
    caption={Template utilizado para gerar o
    Apêndice~\ref{appendix:guia-de-referencia}},
    label={codigo:template},
    language=TeX,
    breaklines=false
]{../manual/templates/monografia.molde}

\section{Criando uma Nova Instrução}%
\label{sec:nova_inst}
O processo de adição de uma instrução é bastante simples: primeiro é necessário
definir qual o comportamento esperado para a nova instrução. Suponhamos que a
nova instrução desejada deva calcular o módulo da subtração \texttt{ry - rz},
salvando o resultado em \texttt{rx}. Para implementar tal instrução bastariam as
adições listadas nos Códigos~\ref{codigo:abs}~e~\ref{codigo:abs_inst}.

\begin{codigo}[%
    caption={Adição ao arquivo bis/instructions/arithmetic.lua},
    label={codigo:abs},
    language={[5.3]lua},
    breaklines=false
]
abs = {
    name = 'Absolute Distance',
    format = 'R3',

    opcode = 0x0B,
    cycles = 1,

    description = [[
        Calculates the absloute distance between
        ry and rz and saves the result into rx.
    ]],
    effect = [[
        if (ry > rz) {
            rx <- ry - rz;
        } else {
            rx <- rz - ry;
        }
        pc <- pc + size;
        cc <- cc + cycles;
    ]],
    status_register = {'z'},

    -- Optional example
    example = [[
        addi r1, zr, -3
        addi r2, zr, 2
        abs r0, r1, r2 ; r0 <- 5
    ]]
}
\end{codigo}

\begin{codigo}[%
    caption={Adição ao arquivo emulator/instructions.c},
    label={codigo:abs_inst},
    language=C,
    breaklines=false
]
BIS_INSTRUCTION(abs) {
    bis_register_t R = RY > RZ ? RY - RZ : RZ - RY;

    PC = PC + BIS_ABS_SIZE;
    CC = CC + BIS_ABS_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    else CLEAR_BIT(FR, BIS_FR_Z);

    RX = R;
}
\end{codigo}

No Código~\ref{codigo:abs} as definições arquiteturais da nova instrução foram
descritas como uma entrada na tabela de instruções. No
Código~\ref{codigo:abs_inst} é definida a implementação da nova instrução para o
emulador. Observe que a implementação usa macros (geradas pelo \emph{script}
\texttt{bis/gen.lua}) para obter acesso aos valores relevantes definidos no
Código~\ref{codigo:abs}. Com essas adições tanto o montador quanto o emulador
estão prontos para lidar com a nova instrução.


\section{Resultados}
As ferramentas atuais apesar de possuírem algumas limitações (discutidas na
Seção~\ref{sec:limita}) já são capazes de produzir resultados interessantes. O
Código~\ref{codigo:hello} a seguir mostra um exemplo de uma variação do clássico
\emph{Hello World!}, que ilustra diversos os aspectos básicos do montador como
\emph{labels}, diretrizes e chamadas de sistema.

\lstinputlisting[%
    style=abnt_codestyle,
    caption={Hello World na arquitetura BIS},
    label={codigo:hello},
    breaklines=false
]{../tests/hello.bis}

Esse exemplo pode ser montado através do comando: ``\texttt{./bis-asembler
tests/hello.bis -o output}'' o arquivo de saída produzido pode ser executado no
emulador através do seguinte comando: ``\texttt{./bis-emulator output}''.

Para compilar o emulador basta executar o comando ``\texttt{make}'', por padrão
os acessos à memória de programa não são verificados. Para ativar as
verificações de segurança em tempo de execução é necessário definir a macro
``\texttt{BIS\_SAFETY\_CHECK}'', o que pode ser feito durante a invocação do
``\texttt{make}'' com o seguinte comando:
``\texttt{CPPFLAGS=-DBIS\_SAFETY\_CHECK make}''. Note que a ativação desta macro
implica uma penalidade de performance.\footnote{A diferença de performance foi
de \(\approx79\)~MIPS (sem verificação) para \(\approx50\)~MIPS (com
verificação).}


\section{Dificuldades e Limitações}%
\label{sec:limita}
No momento, a maior limitação encontra-se no montador, cuja reportagem de erros
ainda não foi implementada. Atualmente a montagem é interrompida no primeiro
erro, indicando apenas se o mesmo foi detectado na etapa de \emph{parsing} ou na
etapa de montagem.

Essa limitação pode ser resolvida no futuro através, do pacote
\emph{lpeglabels}, que permite a introdução de \emph{labels} e pontos de
sincronia na gramática do \emph{parser}. Dessa forma, o tipo do erro pode ser
especificado com mais detalhes enquanto o processo de montagem pode continuar
mesmo após a ocorrência de múltiplos erros.

Outra limitação é a falta de interrupções, apesar da infraestrutura de
registradores estar presente na arquitetura, o emulador ainda não possui nenhum
tipo de tratamento de sinais, logo, não é possível invocar interrupções ainda.
