Neste capítulo, a arquitetura proposta será apresentada e descrita em detalhe.
Considerando que a característica fundamental dela é sua simplicidade, é
apropriado nomeá-la de acordo: \emph{Bis Is Simple} ou \emph{BIS}.

\emph{BIS} foi desenvolvida tomando como inspiração aspectos de diversas outras
arquiteturas, mantando-se sempre o objetivo central de que ela fosse simples e
didática. As seções seguintes descrevem cada um de seus componentes e seu
funcionamento.

\section{Diagrama de Blocos}
\begin{figure}[htb]
    \caption{Diagrama de blocos simplificado.}%
    \label{figura:diagrama}
    \centering
    \includegraphics[scale=.75]{images/diagrama.pdf}
    \fautor%
\end{figure}

Esse diagrama simplificado é útil para descrever o comportamento e a organização
lógica dos componentes. Nele podemos observar que \emph{BIS} é uma arquitetura
\emph{load/store} com duas memórias separadas, mais detalhes sobre a memória são
discutidos na Seção~\ref{sec:memoria}.

Apesar de não ilustrado no desenho simplificado alguns registradores (como o
\texttt{pc} por exemplo) são acessados em todas as operações pela unidade de
controle, porém se um desses registradores for usado como destino de uma
operação a escrita do resultado tem prioridade sobre o comportamento padrão.

\section{Unidade Lógica e Aritmética}
A unidade lógica e aritmética (\emph{ULA}) é capaz de receber até 3 operandos
(de 32~bits cada) e produzir resultados de até 64~bits, divididos em duas saídas
de 32~bits cada. Após a maioria das operações, ela produz também \emph{flags}
úteis para a interpretação do resultado, que são automaticamente armazenadas no
registrador especial denominado \emph{flag register}, descrito com mais detalhes
na Seção~\ref{sec:flag_register}.

\section{Registradores}
O banco de registradores possuí 64 registradores de 32~bits cada: 32
registradores de propósito geral (\emph{GPRs}), 16 registradores reservados para
uso futuro pela \emph{FPU} (\emph{FPRs}), 10 registradores especiais
(\emph{SPRs}) e 6 registradores ``extras''.\footnote{Esses registradores são
funcionalmente idênticos aos registradores de propósito geral, eles estão
presentes para tornar o total de registradores ``redondo''. Futuramente, se
necessário, eles podem ser tomados de volta para uso especial.} Por padrão,
todos os registradores são inicializados com zero.

\begin{quadro}[htb]
\caption{Registradores.}%
\label{quadro:registradores}
\centering
\hspace{3cm}\includegraphics[scale=.75]{registradores}
\fautor%
\end{quadro}

\begin{description}[style=nextline]
    \item[\emph{Program Counter} \texttt{[pc]}]
    Este registrador é responsável por apontar para o endereço da instrução
    atual. Ele é utilizado para endereçar a memória de programa e ler a próxima
    instrução.

    \item[\emph{Instruction Register} \texttt{[irh] [irl]}]
    Esses registradores são responsáveis por conter a instrução atual. Sendo que
    \texttt{irh} possuí os dados apontados por \texttt{pc}, e \texttt{irl}
    possuí os dados apontados por \texttt{pc + 4}. Para instruções de apenas uma
    palavra \texttt{irl} ainda é carregado, porém ignorado.

    \item[\emph{Stack Pointer} \texttt{[sp]}]
    Este registrador aponta para o primeiro byte livre no topo da pilha. Para
    usar a pilha corretamente esse registrador deve ser inicializado o quanto
    antes para o fim da memória de dados.

    \item[\emph{Frame Pointer} \texttt{[fp]}]
    Este registrador é utilizado durante chamadas de métodos para permitir
    acessos à variáveis locais independente da posição atual do topo da pilha.
    Note que a manipulação do \texttt{fp} é responsabilidade do programador ou
    do compilador.

    \item[\emph{Stack Limit Register} \texttt{[slr]}]
    Este registrador é utilizado para evitar \emph{stack overflows}. Quando o
    \emph{stack pointer} ultrapassa o \emph{Stack Limit Register} uma falha de
    memória é registrado e a execução interrompida. Note que como por padrão os
    registradores são inicializados com zero este registrador deve ser
    inicializado com um valor seguro quanto antes.

    \item[\emph{Flag Register} \texttt{[fr]}]
    Este registrador possuí uma série de bits descrevendo diferentes aspectos do
    estado atual da \emph{CPU}. Para mais detalhes sobre este registrador, veja
    a Seção~\ref{sec:flag_register} a seguir.

    \item[\emph{Zero Register} \texttt{[zr]}]
    Este registrador sempre possuí o valor constante zero.

    \item[\emph{Interrupt Mask} \texttt{[im]}]
    Cada bit deste registrador pode ser utilizado para ativar ou desativar uma
    interrupção. As interrupções podem também ser globalmente ativadas ou
    desativadas através do \emph{interrupt flag} presente no \emph{flag
    register}.

    \item[\emph{Cycle Counter} \texttt{[cc]}]
    Este registrador é incrementado uma vez a cada ciclo que a máquina executa.
    Ele pode ser lido ou alterado que nem qualquer outro registrador. Note que
    quando usado como destino de uma operação o resultado da instrução tem
    prioridade sobre o incremento do valor atual.
\end{description}

\subsection{Flag Register}%
\label{sec:flag_register}

\begin{quadro}[htb]
\caption{Flags de Status.}%
\label{quadro:flags}
\centering
\includegraphics[scale=1]{flags}
\fautor%
\end{quadro}

\begin{description}[style=nextline]
    \item[\emph{Zero Flag} \texttt{[z]}:]
    Esta \emph{flag} é utilizada pela \emph{ULA} para indicar quando uma
    operação teve resultado zero. Note que para operações com dois registradores
    de saída \textbf{ambos} devem ser zero para essa \emph{flag} ser ativada.

    \item[\emph{Carry Flag} \texttt{[c]}:]
    Esta flag é utilizada pela \emph{ULA} como um indicador de que o resultado
    de uma operação não pôde ser representado por apenas uma palavra. Esse bit
    também é utilizado por algumas instruções para permitir operações de
    comprimento arbitrário. Note que instruções que produzem duas palavras como
    resultado não usam esta \emph{flag}.

    \item[\emph{Negative Flag} \texttt{[n]}:]
    Esta \emph{flag} essencialmente é igual ao bit mais significativo do
    resultado da \emph{ULA}. Quando ativa ela indica que o resultado
    (interpretado como um complemento de dois) é menor que zero.

    \item[\emph{Overflow Flag} \texttt{[o]}:]
    Esta \emph{flag} é utilizada pela \emph{ULA} como indicador de que uma
    operação \textbf{com sinal} produziu um resultado incorreto devido a falta
    de espaço para o bit de sinal, que foi sobrescrito pelo bit mais
    significativo do resultado.\footnote{%
    Para uma explicação mais detalhada sobre a diferença entre as
    \emph{flags} de \emph{overflow} e \emph{carry} veja~\cite{ian:site}.}

    \item[\emph{Transfer Bit} \texttt{[t]}:]
    Este bit é usado pelas operações de \texttt{shift} para escolher o valor dos
    bits deslocados para dentro. Após a operação o \emph{transfer bit} recebe o
    valor do primeiro bit deslocado para fora. Isso permite o deslocamento de
    dados arbitrariamente longos (um bit de cada vez) através da execução
    múltiplos \texttt{shift}\textsubscript{s} em sequencia. Além disso esse bit
    pode ser usado como armazenamento temporário ou como condição para um desvio.

    \item[\emph{Interrup Mask} \texttt{[i]}:]
    Este bit, quando ativo (\texttt{fr:i == 1}), impede que a sejam executadas
    interrupções.

    \item[\emph{Endianess Bit} \texttt{[e]}:]
    Este bit é usado para configurar a organização dos acessoas a memória.
    Quando ativo~(\texttt{1}), este bit indica que os acessos a memória devem ser
    realizados de forma \emph{big endian}, caso contrário~(\texttt{0}), a memória é
    lida de forma \emph{little endian}. Note que essa \emph{flag} diz respeito
    apenas à memória. Os registradores são \textbf{sempre} \emph{big endian}.
\end{description}

\section{Memória}%
\label{sec:memoria}
Como se pode notar pelo diagrama \emph{BIS} é uma arquitetura de \emph{Harvard}.
Essa decisão se dá principalmente pelo fato de que com memórias separadas é
possível acessar as instruções e os dados em paralelo, reduzindo assim o número
de ciclos necessários em cada operação. Além disso, a separação dos espaços de
memória evita a sobrescrita acidental da memória de programa, já que ela só pode
ser escrita pelo \emph{loader} antes do início da execução.

\emph{BIS} é \emph{bi-endian}, o que permite que ela leia e escreva em ambos
estilos. A \emph{endianess} da \emph{CPU} pode ser configurada em tempo de
execução através do \emph{endianess bit} presentes no \emph{flag
register}~(\ref{sec:flag_register}).

O tamanho de ambas memórias é configurável, limitado apenas pela quantia
disponível no sistema hospedeiro. O tamanho também pode ser propositalmente
reduzido para simular as condições de sistemas embarcados com pouca memória. A
organização da memória de dados é bastante simples, conforme ilustrando na
Figura~\ref{figura:memoria}.

\begin{figure}[htb]
    \caption{Diagrama da memória de dados.}%
    \label{figura:memoria}
    \centering
    \begin{bytefield}{16}
        \wordbox{2}{.data}\\
        \wordbox{3}{Espaço Livre\\\small(Dados)}\\
        \wordbox{1}{\texttt{slr}}\\
        \wordbox{3}{Espaço Livre\\\small(Pilha)}\\
        \wordbox[lrb]{1}{\texttt{sp}} \\
        \wordbox{2}{Pilha\\\small Cresce em direção aos dados}
    \end{bytefield}
    \fautor%
\end{figure}

A seção de dados fica no topo (endereços mais baixos), enquanto a
pilha fica no fundo\footnote{Na prática a pilha pode ficar em qualquer parte da
memória, porém ela sempre crescerá para cima, dessa forma o melhor lugar para se
inicializar a pilha é exatamente no fim da memória.}, crescendo em direção a
seção de dados. O \emph{Stack Limit Register} atua como um divisor para proteger
a memória de dados de ser sobrescrita acidentalmente pela pilha. Quando o
\emph{sp} atinge o \emph{slr} uma falha de memória é registrada e a execução é
interrompida.

\section{Conjunto de Instruções}
O conjunto de instruções está documentado em detalhe nos
Apêndices~\ref{appendix:tabela-de-instrucoes}~e~\ref{appendix:guia-de-referencia},
presente no final deste documento. As instruções estão organizadas em
\textbf{tipos} e \textbf{classes}: o tipo de uma instrução diz respeito ao seu
formato, ou seja, como ela é decodificada e interpretada durante sua execução. A
classe de uma instrução, por outro lado, é uma divisão puramente organizacional,
usada para agrupar instruções de acordo com suas funcionalidades e facilitar a
sua memorização.

\subsection{Formatos de Instrução}
O Quadro~\ref{quadro:formatos} a seguir ilustra cada um dos tipos de instrução.
Observe que valores presentes em mais de um formato (como o \texttt{opcode} ou
\texttt{rx}) sempre aparecem na mesma posição para simplificar o processo de
decodificação.

\begin{quadro}[htb]
\caption{Formatos de Instrução}%
\label{quadro:formatos}
\centering
\hspace*{-1cm}\includegraphics[width=1.1\textwidth]{formatos}
\fautor%
\end{quadro}

\begin{description}[style=nextline]
    \item[\emph{Registrador} \texttt{[R]}:]%
    Instruções do tipo \emph{registrador} operam sobre até 4 registradores,
    identificados por 6~bits cada. Instruções que operam com menos de 4
    registradores ignoram o conteúdo das posições não utilizadas. O registrador
    \texttt{rx} é sempre usado como destino, enquanto \texttt{rz} e \texttt{rw}
    são sempre operandos. A função do registrador \texttt{ry} varia de acordo
    com a operação.

    \item[\emph{Imediata} \texttt{[I]}:]%
    Instruções do tipo \emph{imediata} operam sobre até 3 registradores e um
    valor imediato de 32~bits. Instruções que operam com menos de 3
    registradores ignoram o conteúdo das posições não utilizadas. A função de
    cada registrador e do valor imediato depende da operação.

    \item[\emph{Desvio Absoluto} \texttt{[J]}:]%
    Instruções do tipo \emph{desvio absoluto} possuem apenas dois operandos: uma
    condição\footnote{\label{foot:cond}Veja a Seção~\ref{sec:controle_de_fluxo}
    para mais detalhes sobre os códigos de cada condição.} de 5~bits e um
    endereço imediato de 32~bits, ambos obrigatórios. O endereço é utilizado
    para realizar o desvio caso a condição dada seja verdadeira.

    \item[\emph{Desvio Relativo} \texttt{[RJ]}:]%
    Instruções do tipo \emph{desvio relativo} possuem dois operandos: uma
    condição\cref{foot:cond} de 5~bits e um deslocamento de 20~bits. O
    deslocamento é feito relativo ao \texttt{pc} e é interpretado como
    quantidade de palavras. Essa técnica é inspirada na instrução de
    deslocamento da arquitetura \emph{MIPS}~\cite{manual:mips}.

    \item[\emph{Desvio Indireto} \texttt{[IJ]}:]%
    Instruções do tipo \emph{desvio indireto} possuem três operandos: uma
    condição\cref{foot:cond} de 5~bits e dois registradores. Caso a condição
    dada seja verdadeira os registradores são somados e o resultado é usado como
    destino do desvio.

    \item[\emph{Movbit} \texttt{[M]}:]%
    O formato de instrução \emph{movbit} foi especialmente feito para a
    instrução \texttt{movbit}. A posição dos registradores \texttt{rx} e
    \texttt{ry} se mantem a mesma, porém dois valores (\texttt{j} e \texttt{k})
    são passados em seguida.
\end{description}

\subsection{Aritméticas}
As quatro operações aritméticas básicas estão disponíveis com nomes bastante
intuitivos. A soma e subtração não precisam de instruções separadas para números
com ou sem sinal, porém para as outras operações o sufixo ``\texttt{s}'' ou
``\texttt{u}'' indica se a operação é com ou sem sinal, respectivamente.

As instruções aritméticas dividem-se em dois tipos: \emph{registrador}, com
prefixo \texttt{0x0}, e \emph{imediata}, com prefixo \texttt{0x1}. Elas possuem
funcionalidades análogas, exceto pelas instruções \texttt{sub}/\texttt{subi},
onde a versão imediata da instrução inverte a ordem dos operadores, já que a
operação \texttt{rx \(\leftarrow\) ry \(-\) imediato} pode ser realizada através
da instrução \texttt{addi rx, ry, -n}, por isso a instrução \texttt{subi} trata
o valor imediato como minuendo e o registrador como subtraendo.

\subsection{Lógicas}
As operações lógicas bit-a-bit típicas (\emph{e}, \emph{ou}, \emph{não} e
\emph{ou exclusivo}) estão disponíveis com seus nomes usuais, além das operações
de shift e rotação.

A divisão dos tipos das operações lógica é similar a das aritméticas, sendo o
prefixo \texttt{0x4} dedicado às instruções do tipo \emph{registrador} e o
prefixo \texttt{0x5} às \emph{imediatas}.

\subsection{Controle de Fluxo}%
As instruções de controle de fluxo são formadas pelas operações \texttt{jmp} e
\texttt{call}, junto com suas variantes relativas e indiretas. Todas elas
recebem obrigatoriamente uma \emph{condição} de 5~bits, usada para determinar se
o desvio será ou não realizado. O Quadro~\ref{quadro:desvios} a seguir mostra as
condições disponíveis.

\label{sec:controle_de_fluxo}
\begin{quadro}[htb]
\caption{Condições de Desvio}%
\label{quadro:desvios}
\centering
\includegraphics[scale=.75]{desvios}
\fautor%
\end{quadro}

Observe que as condições opostas estão sempre na mesma linha, dessa forma, o bit
mais significativo da tabela efetivamente inverte a condição. Essa propriedade é
útil, pois, ela simplifica o processo de verificação das condições, bastando
verificar metade da tabela e negar o resultado caso o bit mais significativo
seja 1.

\subsection{Load/Store}
As instruções \emph{Load/Store} estão divididas em dois tipos, de maneira
similar às instruções aritméticas, com o prefixo \texttt{0x6} dedicado às do tipo
\emph{registrador} e \texttt{0x7} para as \emph{imediatas}. Os acessos à memoria
podem ser feitos com em tamanhos de 8, 16, 32 e 64 bits. Os acessos com mais de
um byte seguem a ordem imposta pelo \emph{endianess flag}
(\ref{sec:flag_register}).

\subsection{Misc}
A classe de instruções \emph{Misc} comporta instruções diversas que não se
encaixam bem em nenhuma das outras classes. Nela encontram-se instruções como
\texttt{swap} (troca de registradores) ou \texttt{break} (parada de execução
para debug).

\subsection{Chamadas de Sistema}
Apesar da arquitetura \emph{BIS} não possuir um sistema de execução privilegiada
a instrução de chamada de sistema \texttt{syscall} ainda foi adicionada, pois ela
oferece uma interface bastante útil e prática para realizar operações complexas
como entrada e saída formatadas. O Quadro~\ref{quadro:syscalls} a seguir mostra
as chamadas de sistema disponíveis atualmente.

\begin{quadro}[htb]
\caption{Chamadas de Sistema}%
\label{quadro:syscalls}
\centering
\includegraphics[scale=.85]{syscalls}
\fautor%
\end{quadro}
