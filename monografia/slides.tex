\documentclass[15pt]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{courier}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{bytefield}

\graphicspath{{./images/}}

\usetheme{Copenhagen}
\beamertemplatenavigationsymbolsempty
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\lstset{basicstyle=\small\ttfamily,numbers=left}

\newcommand{\fautor}{\mbox{\tiny Fonte: Elaborada pelo autor.}}

\title[\emph{BIS} --- \emph{Bis Is Simple}]{%
Proposta de Redesign para o Processador Didático do ICMC}
\subtitle{\emph{BIS} --- \emph{Bis Is Simple}}
\author[Nascimento, P. B. N.]{Paulo Bardes Nogueira Nascimento}
\institute[ICMC --- USP]{%
  Instituto de Ciências Matemáticas e de Computação\\
  Universidade de São Paulo
}
\date{4 de Dezembro de 2017}

\logo{\includegraphics[height=7mm]{logo-capa}}

\begin{document}
\frame{\titlepage}

\section{Introdução}
\subsection{Motivação}
\begin{frame}\frametitle{Motivação}
A ideia inicial para este projeto surgiu durante uma conversa casual sobre os
cursos de introdução a programação, e como para muitos programadores
computadores eram simplesmente ``mágicos''.

Os cursos de arquitetura e organização de computadores são justamente uma
oportunidade para os alunos quebrarem esse modelo \emph{caixa preta} de uma
\emph{CPU} e investigar como exatamente funciona um computador.

\begin{itemize}
    \item Curiosidade
    \item Desafio
    \item Frustração com ferramentas antigas
\end{itemize}
\end{frame}

\subsection{Objetivos}
\begin{frame}\frametitle{Objetivos}
O objetivo primário deste projeto é criar um conjunto de ferramentas ideal para
o ensino das disciplinas de arquitetura e organização de computadores.
\begin{itemize}
    \item Simples
    \item Fácil utilização
    \item Fácil manutenção e extensão
    \item Boa documentação
    \item Desempenho Razoável
\end{itemize}
\end{frame}

\section{A Arquitetura}
\subsection{Características}
\begin{frame}\frametitle{Características}
\emph{BIS} foi inspirada em parte pela atual arquitetura do
\emph{Processador-ICMC}, mantendo-se onde possível as convenções de nomes já
estabelecidas, porém introduzindo diversas novas características:
\begin{multicols}{2}
    \begin{itemize}
        \item Arquitetura de \emph{Harvard}
        \item \emph{Bi-Endian}
        \item \emph{RISC}
        \item 32-bits
        \item Argumentos Imediatos
        \item Documentação Automática
        \item 32\footnote{Mais 6 registradores ``extras''.} \emph{GPRs}
    \end{itemize}
\end{multicols}
\end{frame}

\subsection{Instruções}
\begin{frame}\frametitle{Instruções}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{instrucoes}\\
    \fautor
\end{figure}
\end{frame}

\subsection{Memória}
\begin{frame}\frametitle{Memória}
\begin{figure}[htb]
    \centering
    \begin{bytefield}{16}
        \wordbox{1}{.data}\\
        \wordbox{2}{Espaço Livre\\\small(Dados)}\\
        \wordbox{1}{\texttt{slr}}\\
        \wordbox{2}{Espaço Livre\\\small(Pilha)}\\
        \wordbox[lrb]{1}{\texttt{sp}} \\
        \wordbox{2}{Pilha\\\small Cresce em direção aos dados}
    \end{bytefield}\\
    \fautor
\end{figure}
\end{frame}

\subsection{Registradores}
\begin{frame}\frametitle{Registradores}
\begin{figure}
    \begin{minipage}[c]{0.65\linewidth}
        \centering
        \includegraphics[width=\textwidth]{registradores}
        \fautor
    \end{minipage}%
    \hfill%
    \begin{minipage}[c]{0.3\linewidth}
        \centering
        \includegraphics[width=\textwidth]{flags}
        \fautor
    \end{minipage}
\end{figure}
\end{frame}

\section{Resultados}
\subsection{Emulador}
\begin{frame}\frametitle{Emulador}
O emulador foi desenvolvido em \emph{C} devido principalmente a necessidade de
mais controle no ``baixo nível''. Os dados arquiteturais foram acessados através
de macros e tabelas de consulta geradas pelo script ``\texttt{bis/gen.lua}''.
\\[1em]

Exemplo de uso:\\
\texttt{./bis-emulator hello.out}

\begin{multicols}{2}
    \begin{itemize}
        \item Fácil alteração/adição de instruções
        \item \(\approx 50\)\emph{MIPS}\footnote{Com verificações de acesso a
        memória de programa.}
        \item Padrão \emph{C11}
        \item Compilação automatizada via \texttt{make}
    \end{itemize}
\end{multicols}
\end{frame}

\subsection{Montador}
\begin{frame}\frametitle{Montador}
O montador foi desenvolvido inteiramente em \emph{Lua}, o que permite que os
dados arquiteturais sejam consultados em tempo de montagem.\\[1em]

Exemplo de uso:\\
\texttt{./bis-assembler hello.bis -o hello.out}

\begin{multicols}{2}
    \begin{itemize}
        \item Macros \emph{Lua}
        \item Gramática \emph{lpeg}
        \item Duas passagens
        \item Labels e símbolos
    \end{itemize}
\end{multicols}
\end{frame}

\subsection{Documentação}
\begin{frame}\frametitle{Documentação}
\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{doc}
    \fautor
\end{figure}
\end{frame}

\subsection{Exemplo}
\begin{frame}[fragile]\frametitle{Exemplo}
\begin{tabular}{c}
\begin{lstlisting}
addi r0, zr, 1000
addi r1, zr, 4 ; Chosen by fair dice roll.
               ; Guarenteed to be random.
loop_in:
    shiftli r2, r1, 13
    xor r1, r1, r2
    shiftri r2, r1, 15
    xor r1, r1, r2
    shiftli r2, r1, 5
    xor r1, r1, r2

    addi r0, r0, -1
    jmp nz, loop_in
\end{lstlisting}
\end{tabular}
\end{frame}

\section{Conclusão}
\begin{frame}\frametitle{Conclusão}
O desenvolvimento de uma arquitetura didática se mostrou bastante desafiador,
porém gratificante. Atualmente as ferramentas disponíveis já oferecem uma opção
bastante prática para o ensino de conceitos fundamentais para a disciplína de
arquitetura de computadoresde computadores.

Diversas áreas ainda podem se beneficiar de trabalhos futuros:
\begin{multicols}{2}
    \begin{itemize}
        \item Reportagem de erros
        \item Interrupções
        \item Controlador de Vídeo
        \item Entrada e saída mapeada em memória
        \item \emph{IDE}
        \item Implementação em \emph{FPGA}
    \end{itemize}
\end{multicols}
\end{frame}

\end{document}
