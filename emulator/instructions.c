#include "bis.h"

#include <errno.h>
#include <inttypes.h>
#include <stdlib.h>

#include "utils.h"
#include "instructions.h"

/* Defines a few commonly used values */
#define RX (M->registers[args->rx])
#define RY (M->registers[args->ry])
#define RZ (M->registers[args->rz])
#define RW (M->registers[args->rw])
#define R0 (M->registers[0])
#define R1 (M->registers[1])
#define R2 (M->registers[2])
#define R3 (M->registers[3])
#define FR (M->registers[BIS_FR_REG])
#define PC (M->registers[BIS_PC_REG])
#define CC (M->registers[BIS_CC_REG])
#define SP (M->registers[BIS_SP_REG])
#define IRH (M->registers[BIS_IRH_REG])
#define IRL (M->registers[BIS_IRL_REG])
#define J (args->j)
#define K (args->k)
#define IMMEDIATE   (args->immediate)
#define ADDRESS     (args->address)
#define CONDITION   (args->condition)
#define OFFSET      (args->offset)

/* Single bits from the FR.
 * Note that GET_BIT always puts the desired bit in LSB. */
#define FR_Z GET_BIT(FR, BIS_FR_Z)
#define FR_C GET_BIT(FR, BIS_FR_C)
#define FR_O GET_BIT(FR, BIS_FR_O)
#define FR_N GET_BIT(FR, BIS_FR_N)
#define FR_I GET_BIT(FR, BIS_FR_I)
#define FR_T GET_BIT(FR, BIS_FR_T)
#define FR_E GET_BIT(FR, BIS_FR_E)

/* Memory access macros */
#define DATA_MEMORY(offset) (M->memory_state.data_memory.bytes + offset)
#define PROGRAM_MEMORY(offset) (M->memory_state.program_memory.bytes + offset)

/* Verifies if the given condition is true given the state M. */
static int verify_condition(const bis_machine_state_t *M, uint8_t condition)
{
    uint64_t result;
    switch(condition & 0xF) { /* Note that the first bit is ignored here
                               * to make the code more efficient. */

        /* Unconditional jump */
        case 0U: return 1;

        /* Here be crazy bit hacking */
        case BIS_UGR_COND: result = FR_C & ~FR_Z; break;
        case BIS_ULE_COND: result = ~(FR_C | FR_Z); break;
        case BIS_SGR_COND: result = ~((FR_O ^ FR_N) | FR_Z); break;
        case BIS_SLE_COND: result = ~FR_Z & (FR_O ^ FR_N); break;

        /* These are pretty straight forward... */
        case BIS_Z_COND: result = FR_Z; break;
        case BIS_C_COND: result = FR_C; break;
        case BIS_O_COND: result = FR_O; break;
        case BIS_N_COND: result = FR_N; break;

        default:
            ERROR_MSG("Unexpected condition code 0x%02hhX.", condition);
            //TODO: LOG_MACHINE_STATE(FILE *f)
            return 0;
    }

    /* Clears all but the LSB of the result and flips it if we want the
     * opposite condition of what was tested. */
    return (int) ((result & 1LLU) ^ GET_BIT(condition, 4));
}

/*****************************************************************************
*                                Arithmetic                                  *
*****************************************************************************/


/* Note that RX is set last to make sure that if RX is set to PC, CC or FR the
 * result of the operation will take precedence over the "default" behavior for
 * that register. */
BIS_INSTRUCTION(add) {
    uint64_t R = (uint64_t) RY + (uint64_t) RZ;

    PC = PC + BIS_ADD_SIZE;
    CC = CC + BIS_ADD_CYCLES;

    MOV_BIT(FR, BIS_FR_Z, (bis_register_t) R == 0, 0);
    MOV_BIT(FR, BIS_FR_C, R, 32);
    MOV_BIT(FR, BIS_FR_N, R, 31);
    MOV_BIT(FR, BIS_FR_O, ~(RY ^ RZ) & (RZ ^ R), 31);

    RX = (bis_register_t) R;
}

BIS_INSTRUCTION(addc) {
    uint64_t R = (uint64_t) RY + (uint64_t) RZ + GET_BIT(FR, BIS_FR_C);

    PC = PC + BIS_ADDC_SIZE;
    CC = CC + BIS_ADDC_CYCLES;

    MOV_BIT(FR, BIS_FR_Z, (bis_register_t) R == 0, 0);
    MOV_BIT(FR, BIS_FR_C, R, 32);
    MOV_BIT(FR, BIS_FR_N, R, 31);
    MOV_BIT(FR, BIS_FR_O, ~(RY ^ RZ) & (RZ ^ R), 31);

    RX = (bis_register_t) R;
}

BIS_INSTRUCTION(sub) {
    uint64_t R = (uint64_t) RY - (uint64_t) RZ;

    PC = PC + BIS_SUB_SIZE;
    CC = CC + BIS_SUB_CYCLES;

    MOV_BIT(FR, BIS_FR_Z, (bis_register_t) R == 0, 0);
    MOV_BIT(FR, BIS_FR_C, R, 32);
    MOV_BIT(FR, BIS_FR_N, R, 31);
    MOV_BIT(FR, BIS_FR_O, ~(RY ^ RZ) & (RZ ^ R), 31);

    RX = (bis_register_t) R;
}

BIS_INSTRUCTION(subc) {
    uint64_t R = (uint64_t) RY - (uint64_t) RZ - GET_BIT(FR, BIS_FR_C);

    PC = PC + BIS_SUBC_SIZE;
    CC = CC + BIS_SUBC_CYCLES;

    MOV_BIT(FR, BIS_FR_Z, (bis_register_t) R == 0, 0);
    MOV_BIT(FR, BIS_FR_C, R, 32);
    MOV_BIT(FR, BIS_FR_N, R, 31);
    MOV_BIT(FR, BIS_FR_O, ~(RY ^ RZ) & (RZ ^ R), 31);

    RX = (bis_register_t) R;
}

BIS_INSTRUCTION(mulu) {
    uint64_t R = (uint64_t) RZ * (uint64_t) RW;

    PC = PC + BIS_MULU_SIZE;
    CC = CC + BIS_MULU_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = (bis_register_t) (R >> 32);
    RY = (bis_register_t) R;
}

BIS_INSTRUCTION(muls) {
    int64_t R = (int64_t) RZ * (int64_t) RW;

    PC = PC + BIS_MULS_SIZE;
    CC = CC + BIS_MULS_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    if(R < 0)  SET_BIT(FR, BIS_FR_N);

    RX = (bis_register_t) (R >> 32);
    RY = (bis_register_t) R;
}

BIS_INSTRUCTION(divu) {
    FAIL_MSG(RZ == 0, "Division by zero detected! Doing nothing instead. "
    "(PC: %08X)", PC);

    bis_register_t R = RY / RZ;

    PC = PC + BIS_DIVU_SIZE;
    CC = CC + BIS_DIVU_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
    return;

fail:
    PC = PC + BIS_DIVU_SIZE;
    CC = CC + BIS_DIVU_CYCLES;
}

BIS_INSTRUCTION(divs) {
    FAIL_MSG(RZ == 0, "Division by zero detected! Doing nothing instead. "
    "(PC: %08X)", PC);

    int32_t R = (int32_t) RY / (int32_t) RZ;

    PC = PC + BIS_DIVS_SIZE;
    CC = CC + BIS_DIVS_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    if(R < 0)  SET_BIT(FR, BIS_FR_N);

    RX = (bis_register_t) R;
    return;

fail:
    PC = PC + BIS_DIVS_SIZE;
    CC = CC + BIS_DIVS_CYCLES;
}

BIS_INSTRUCTION(modu) {
    bis_register_t R = RY % RZ;

    PC = PC + BIS_MODU_SIZE;
    CC = CC + BIS_MODU_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
}

BIS_INSTRUCTION(mods) {
    int32_t R = (int32_t) RY % (int32_t) RZ;
    if(GET_BIT(RY ^ RZ, 31)) R += RZ;

    PC = PC + BIS_MODS_SIZE;
    CC = CC + BIS_MODS_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    if(R < 0)  SET_BIT(FR, BIS_FR_N);

    RX = (bis_register_t) R;
}

BIS_INSTRUCTION(rems) {
    int32_t R = (int32_t) RY % (int32_t) RZ;

    PC = PC + BIS_REMS_SIZE;
    CC = CC + BIS_REMS_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    if(R < 0)  SET_BIT(FR, BIS_FR_N);

    RX = (bis_register_t) R;
}

BIS_INSTRUCTION(addi) {
    uint64_t R = (uint64_t) RY + (uint64_t) IMMEDIATE;

    PC = PC + BIS_ADDI_SIZE;
    CC = CC + BIS_ADDI_CYCLES;

    MOV_BIT(FR, BIS_FR_Z, (bis_register_t) R == 0, 0);
    MOV_BIT(FR, BIS_FR_C, R, 32);
    MOV_BIT(FR, BIS_FR_N, R, 31);
    MOV_BIT(FR, BIS_FR_O, ~(RY ^ RZ) & (RZ ^ R), 31);

    RX = (bis_register_t) R;
}

BIS_INSTRUCTION(subi) {
    uint64_t R = (uint64_t) IMMEDIATE - (uint64_t) RY;

    PC = PC + BIS_SUBI_SIZE;
    CC = CC + BIS_SUBI_CYCLES;

    MOV_BIT(FR, BIS_FR_Z, (bis_register_t) R == 0, 0);
    MOV_BIT(FR, BIS_FR_C, R, 32);
    MOV_BIT(FR, BIS_FR_N, R, 31);
    MOV_BIT(FR, BIS_FR_O, ~(RY ^ RZ) & (RZ ^ R), 31);

    RX = (bis_register_t) R;
}

BIS_INSTRUCTION(mului) {
    uint64_t R = (uint64_t) RZ * (uint64_t) IMMEDIATE;

    PC = PC + BIS_MULUI_SIZE;
    CC = CC + BIS_MULUI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = (bis_register_t) (R >> 32);
    RY = (bis_register_t) R;
}

BIS_INSTRUCTION(mulsi) {
    int64_t R = (int64_t) RZ * (int64_t) IMMEDIATE;

    PC = PC + BIS_MULSI_SIZE;
    CC = CC + BIS_MULSI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    if(R < 0)  SET_BIT(FR, BIS_FR_N);

    RX = (bis_register_t) (R >> 32);
    RY = (bis_register_t) R;
}

BIS_INSTRUCTION(divui) {
    FAIL_MSG(IMMEDIATE == 0,
    "Division by zero detected! Doing nothing instead. (PC: %08X)", PC);

    bis_register_t R = RY / IMMEDIATE;

    PC = PC + BIS_DIVUI_SIZE;
    CC = CC + BIS_DIVUI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
    return;

fail:
    PC = PC + BIS_DIVUI_SIZE;
    CC = CC + BIS_DIVUI_CYCLES;
}

BIS_INSTRUCTION(divsi) {
    FAIL_MSG(IMMEDIATE == 0,
    "Division by zero detected! Doing nothing instead. (PC: %08X)", PC);

    int32_t R = (int32_t) RY / (int32_t) IMMEDIATE;

    PC = PC + BIS_DIVSI_SIZE;
    CC = CC + BIS_DIVSI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    if(R < 0)  SET_BIT(FR, BIS_FR_N);

    RX = (bis_register_t) R;
    return;

fail:
    PC = PC + BIS_DIVSI_SIZE;
    CC = CC + BIS_DIVSI_CYCLES;
}

BIS_INSTRUCTION(modui) {
    bis_register_t R = RY % IMMEDIATE;

    PC = PC + BIS_MODUI_SIZE;
    CC = CC + BIS_MODUI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
}

BIS_INSTRUCTION(modsi) {
    int32_t R = (int32_t) RY % (int32_t) IMMEDIATE;
    if(GET_BIT(RY ^ IMMEDIATE, 31)) R += IMMEDIATE;

    PC = PC + BIS_MODSI_SIZE;
    CC = CC + BIS_MODSI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    if(R < 0)  SET_BIT(FR, BIS_FR_N);

    RX = (bis_register_t) R;
}

BIS_INSTRUCTION(remsi) {
    int32_t R = (int32_t) RY % (int32_t) IMMEDIATE;

    PC = PC + BIS_REMSI_SIZE;
    CC = CC + BIS_REMSI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    if(R < 0)  SET_BIT(FR, BIS_FR_N);

    RX = (bis_register_t) R;
}

/*****************************************************************************
*                                 Logical                                    *
*****************************************************************************/

/* Note that temporary variable R is needed since RX migth be PC, CC or FR */
BIS_INSTRUCTION(not) {
    bis_register_t R = ~RY;

    PC = PC + BIS_NOT_SIZE;
    CC = CC + BIS_NOT_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
}

BIS_INSTRUCTION(and) {
    bis_register_t R = RY & RZ;

    PC = PC + BIS_AND_SIZE;
    CC = CC + BIS_AND_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
}

BIS_INSTRUCTION(or) {
    bis_register_t R = RY | RZ;

    PC = PC + BIS_OR_SIZE;
    CC = CC + BIS_OR_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
}

BIS_INSTRUCTION(xor) {
    bis_register_t R = RY ^ RZ;

    PC = PC + BIS_XOR_SIZE;
    CC = CC + BIS_XOR_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
}

BIS_INSTRUCTION(shiftl) {
    bis_register_t R = RY << (RZ % 32);
    if(FR_T && RZ % 32) R |= (0xFFFFFFFFU >> (32 - RZ % 32));

    PC = PC + BIS_SHIFTL_SIZE;
    CC = CC + BIS_SHIFTL_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    MOV_BIT(FR, BIS_FR_T, RY, 0);

    RX = R;
}

BIS_INSTRUCTION(shiftr) {
    bis_register_t R = RY >> (RZ % 32);
    if(FR_T && RZ % 32) R |= (0xFFFFFFFFU << (32 - RZ % 32));

    PC = PC + BIS_SHIFTR_SIZE;
    CC = CC + BIS_SHIFTR_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    MOV_BIT(FR, BIS_FR_T, RY, 31);

    RX = R;
}

BIS_INSTRUCTION(rotl) {
    bis_register_t R = RY << (RZ % 32);
    if(RZ % 32) R |= (RY >> (32 - RZ % 32));

    PC = PC + BIS_SHIFTL_SIZE;
    CC = CC + BIS_SHIFTL_CYCLES;

    RX = R;
}

BIS_INSTRUCTION(rotr) {
    bis_register_t R = RY >> (RZ % 32);
    if(RZ % 32) R |= (RY << (32 - RZ % 32));

    PC = PC + BIS_SHIFTL_SIZE;
    CC = CC + BIS_SHIFTL_CYCLES;

    RX = R;
}

BIS_INSTRUCTION(andi) {
    bis_register_t R = RY & IMMEDIATE;

    PC = PC + BIS_ANDI_SIZE;
    CC = CC + BIS_ANDI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
}

BIS_INSTRUCTION(ori) {
    bis_register_t R = RY | IMMEDIATE;

    PC = PC + BIS_ORI_SIZE;
    CC = CC + BIS_ORI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
}

BIS_INSTRUCTION(xori) {
    bis_register_t R = RY ^ IMMEDIATE;

    PC = PC + BIS_XORI_SIZE;
    CC = CC + BIS_XORI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);

    RX = R;
}

BIS_INSTRUCTION(shiftli) {
    bis_register_t R = RY << (IMMEDIATE % 32);
    if(FR_T && IMMEDIATE % 32) R |= (0xFFFFFFFFU >> (32 - IMMEDIATE % 32));

    PC = PC + BIS_SHIFTLI_SIZE;
    CC = CC + BIS_SHIFTLI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    MOV_BIT(FR, BIS_FR_T, RY, 0);

    RX = R;
}

BIS_INSTRUCTION(shiftri) {
    bis_register_t R = RY >> (IMMEDIATE % 32);
    if(FR_T && IMMEDIATE % 32) R |= (0xFFFFFFFFU << (32 - IMMEDIATE % 32));

    PC = PC + BIS_SHIFTRI_SIZE;
    CC = CC + BIS_SHIFTRI_CYCLES;

    if(R == 0) SET_BIT(FR, BIS_FR_Z);
    MOV_BIT(FR, BIS_FR_T, RY, 31);

    RX = R;
}

BIS_INSTRUCTION(rotli) {
    bis_register_t R = RY << (IMMEDIATE % 32);
    if(IMMEDIATE % 32) R |= (RY >> (32 - IMMEDIATE % 32));

    PC = PC + BIS_SHIFTLI_SIZE;
    CC = CC + BIS_SHIFTLI_CYCLES;

    RX = R;
}

BIS_INSTRUCTION(rotri) {
    bis_register_t R = RY >> (IMMEDIATE % 32);
    if(IMMEDIATE % 32) R |= (RY << (32 - IMMEDIATE % 32));

    PC = PC + BIS_SHIFTLI_SIZE;
    CC = CC + BIS_SHIFTLI_CYCLES;

    RX = R;
}


 /*****************************************************************************
 *                               Load / Store                                 *
 *****************************************************************************/

BIS_INSTRUCTION(load64) {
    bis_register_t addr = RZ;
    PC = PC + BIS_LOAD64_SIZE;
    CC = CC + BIS_LOAD64_CYCLES;

    RX = 0; RY = 0;
    if(FR_E) {
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 0)) << 24;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 1)) << 16;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 2)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 3)) << 0;
        RY |=  ((bis_register_t) *DATA_MEMORY(addr + 4)) << 24;
        RY |=  ((bis_register_t) *DATA_MEMORY(addr + 5)) << 16;
        RY |=  ((bis_register_t) *DATA_MEMORY(addr + 6)) << 8;
        RY |=  ((bis_register_t) *DATA_MEMORY(addr + 7)) << 0;
    } else {
        RY |=  ((bis_register_t) *DATA_MEMORY(addr + 0)) << 0;
        RY |=  ((bis_register_t) *DATA_MEMORY(addr + 1)) << 8;
        RY |=  ((bis_register_t) *DATA_MEMORY(addr + 2)) << 16;
        RY |=  ((bis_register_t) *DATA_MEMORY(addr + 3)) << 24;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 4)) << 0;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 5)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 6)) << 16;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 7)) << 24;
    }
}

BIS_INSTRUCTION(store64) {
    bis_register_t data_y = RY, data_z = RZ;
    PC = PC + BIS_STORE64_SIZE;
    CC = CC + BIS_STORE64_CYCLES;

    if(FR_E) {
        *DATA_MEMORY(RX + 0) = (uint8_t) (data_y >> 24);
        *DATA_MEMORY(RX + 1) = (uint8_t) (data_y >> 16);
        *DATA_MEMORY(RX + 2) = (uint8_t) (data_y >> 8);
        *DATA_MEMORY(RX + 3) = (uint8_t) (data_y >> 0);
        *DATA_MEMORY(RX + 4) = (uint8_t) (data_y >> 24);
        *DATA_MEMORY(RX + 5) = (uint8_t) (data_y >> 16);
        *DATA_MEMORY(RX + 6) = (uint8_t) (data_y >> 8);
        *DATA_MEMORY(RX + 7) = (uint8_t) (data_y >> 0);
    } else {
        *DATA_MEMORY(RX + 0) = (uint8_t) (data_z >> 0);
        *DATA_MEMORY(RX + 1) = (uint8_t) (data_z >> 8);
        *DATA_MEMORY(RX + 2) = (uint8_t) (data_z >> 16);
        *DATA_MEMORY(RX + 3) = (uint8_t) (data_z >> 24);
        *DATA_MEMORY(RX + 4) = (uint8_t) (data_z >> 0);
        *DATA_MEMORY(RX + 5) = (uint8_t) (data_z >> 8);
        *DATA_MEMORY(RX + 6) = (uint8_t) (data_z >> 16);
        *DATA_MEMORY(RX + 7) = (uint8_t) (data_z >> 24);
    }
}

BIS_INSTRUCTION(load32) {
    bis_register_t addr = RY;
    PC = PC + BIS_LOAD32_SIZE;
    CC = CC + BIS_LOAD32_CYCLES;

    RX = 0;
    if(FR_E) {
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 0)) << 24;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 1)) << 16;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 2)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 3)) << 0;
    } else {
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 0)) << 0;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 1)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 2)) << 16;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 3)) << 24;
    }
}

BIS_INSTRUCTION(store32) {
    bis_register_t data = RY;
    PC = PC + BIS_STORE32_SIZE;
    CC = CC + BIS_STORE32_CYCLES;

    if(FR_E) {
        *DATA_MEMORY(RX + 0) = (uint8_t) (data >> 24);
        *DATA_MEMORY(RX + 1) = (uint8_t) (data >> 16);
        *DATA_MEMORY(RX + 2) = (uint8_t) (data >> 8);
        *DATA_MEMORY(RX + 3) = (uint8_t) (data >> 0);
    } else {
        *DATA_MEMORY(RX + 0) = (uint8_t) (data >> 0);
        *DATA_MEMORY(RX + 1) = (uint8_t) (data >> 8);
        *DATA_MEMORY(RX + 2) = (uint8_t) (data >> 16);
        *DATA_MEMORY(RX + 3) = (uint8_t) (data >> 24);
    }
}

BIS_INSTRUCTION(load16) {
    bis_register_t addr = RY;
    PC = PC + BIS_LOAD16_SIZE;
    CC = CC + BIS_LOAD16_CYCLES;

    RX = RX & 0xFFFF0000;
    if(FR_E) {
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 0)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 1)) << 0;
    } else {
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 0)) << 0;
        RX |=  ((bis_register_t) *DATA_MEMORY(addr + 1)) << 8;
    }
}

BIS_INSTRUCTION(store16) {
    bis_register_t data = RY;
    PC = PC + BIS_STORE16_SIZE;
    CC = CC + BIS_STORE16_CYCLES;

    if(FR_E) {
        *DATA_MEMORY(RX + 0) = (uint8_t) (data >> 8);
        *DATA_MEMORY(RX + 1) = (uint8_t) (data >> 0);
    } else {
        *DATA_MEMORY(RX + 0) = (uint8_t) (data >> 0);
        *DATA_MEMORY(RX + 1) = (uint8_t) (data >> 8);
    }
}

BIS_INSTRUCTION(load8) {
    bis_register_t addr = RY;
    PC = PC + BIS_LOAD8_SIZE;
    CC = CC + BIS_LOAD8_CYCLES;

    RX = RX & 0xFFFFFF00;
    RX = RX | *DATA_MEMORY(addr);
}

BIS_INSTRUCTION(store8) {
    bis_register_t data = RY;
    PC = PC + BIS_STORE8_SIZE;
    CC = CC + BIS_STORE8_CYCLES;
    *DATA_MEMORY(RX) = (uint8_t) data;
}

BIS_INSTRUCTION(loadi64) {
    PC = PC + BIS_LOADI64_SIZE;
    CC = CC + BIS_LOADI64_CYCLES;

    RX = 0; RY = 0;
    if(FR_E) {
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 0)) << 24;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 1)) << 16;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 2)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 3)) << 0;
        RY |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 4)) << 24;
        RY |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 5)) << 16;
        RY |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 6)) << 8;
        RY |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 7)) << 0;
    } else {
        RY |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 0)) << 0;
        RY |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 1)) << 8;
        RY |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 2)) << 16;
        RY |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 3)) << 24;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 4)) << 0;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 5)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 6)) << 16;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 7)) << 24;
    }
}

BIS_INSTRUCTION(storei64) {
    PC = PC + BIS_STOREI64_SIZE;
    CC = CC + BIS_STOREI64_CYCLES;

    if(FR_E) {
        *DATA_MEMORY(IMMEDIATE + 0) = (uint8_t) (RX >> 24);
        *DATA_MEMORY(IMMEDIATE + 1) = (uint8_t) (RX >> 16);
        *DATA_MEMORY(IMMEDIATE + 2) = (uint8_t) (RX >> 8);
        *DATA_MEMORY(IMMEDIATE + 3) = (uint8_t) (RX >> 0);
        *DATA_MEMORY(IMMEDIATE + 4) = (uint8_t) (RY >> 24);
        *DATA_MEMORY(IMMEDIATE + 5) = (uint8_t) (RY >> 16);
        *DATA_MEMORY(IMMEDIATE + 6) = (uint8_t) (RY >> 8);
        *DATA_MEMORY(IMMEDIATE + 7) = (uint8_t) (RY >> 0);
    } else {
        *DATA_MEMORY(IMMEDIATE + 0) = (uint8_t) (RY >> 0);
        *DATA_MEMORY(IMMEDIATE + 1) = (uint8_t) (RY >> 8);
        *DATA_MEMORY(IMMEDIATE + 2) = (uint8_t) (RY >> 16);
        *DATA_MEMORY(IMMEDIATE + 3) = (uint8_t) (RY >> 24);
        *DATA_MEMORY(IMMEDIATE + 4) = (uint8_t) (RX >> 0);
        *DATA_MEMORY(IMMEDIATE + 5) = (uint8_t) (RX >> 8);
        *DATA_MEMORY(IMMEDIATE + 6) = (uint8_t) (RX >> 16);
        *DATA_MEMORY(IMMEDIATE + 7) = (uint8_t) (RX >> 24);
    }
}

BIS_INSTRUCTION(loadi32) {
    PC = PC + BIS_LOADI32_SIZE;
    CC = CC + BIS_LOADI32_CYCLES;

    RX = 0;
    if(FR_E) {
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 0)) << 24;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 1)) << 16;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 2)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 3)) << 0;
    } else {
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 0)) << 0;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 1)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 2)) << 16;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 3)) << 24;
    }
}

BIS_INSTRUCTION(storei32) {
    PC = PC + BIS_STOREI32_SIZE;
    CC = CC + BIS_STOREI32_CYCLES;

    if(FR_E) {
        *DATA_MEMORY(IMMEDIATE + 0) = (uint8_t) (RX >> 24);
        *DATA_MEMORY(IMMEDIATE + 1) = (uint8_t) (RX >> 16);
        *DATA_MEMORY(IMMEDIATE + 2) = (uint8_t) (RX >> 8);
        *DATA_MEMORY(IMMEDIATE + 3) = (uint8_t) (RX >> 0);
    } else {
        *DATA_MEMORY(IMMEDIATE + 0) = (uint8_t) (RX >> 0);
        *DATA_MEMORY(IMMEDIATE + 1) = (uint8_t) (RX >> 8);
        *DATA_MEMORY(IMMEDIATE + 2) = (uint8_t) (RX >> 16);
        *DATA_MEMORY(IMMEDIATE + 3) = (uint8_t) (RX >> 24);
    }
}

BIS_INSTRUCTION(loadi16) {
    PC = PC + BIS_LOADI16_SIZE;
    CC = CC + BIS_LOADI16_CYCLES;

    RX = RX & 0xFFFF0000;
    if(FR_E) {
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 0)) << 8;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 1)) << 0;
    } else {
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 0)) << 0;
        RX |=  ((bis_register_t) *DATA_MEMORY(IMMEDIATE + 1)) << 8;
    }
}

BIS_INSTRUCTION(storei16) {
    PC = PC + BIS_STOREI16_SIZE;
    CC = CC + BIS_STOREI16_CYCLES;

    if(FR_E) {
        *DATA_MEMORY(IMMEDIATE + 0) = (uint8_t) (RX >> 8);
        *DATA_MEMORY(IMMEDIATE + 1) = (uint8_t) (RX >> 0);
    } else {
        *DATA_MEMORY(IMMEDIATE + 0) = (uint8_t) (RX >> 0);
        *DATA_MEMORY(IMMEDIATE + 1) = (uint8_t) (RX >> 8);
    }
}

BIS_INSTRUCTION(loadi8) {
    PC = PC + BIS_LOADI8_SIZE;
    CC = CC + BIS_LOADI8_CYCLES;

    RX = RX & 0xFFFFFF00;
    RX = RX | *DATA_MEMORY(IMMEDIATE);
}

BIS_INSTRUCTION(storei8) {
    PC = PC + BIS_STOREI8_SIZE;
    CC = CC + BIS_STOREI8_CYCLES;
    *DATA_MEMORY(IMMEDIATE) = (uint8_t) RX;
}

/*****************************************************************************
*                               Flow Control                                 *
*****************************************************************************/

BIS_INSTRUCTION(jmp) {
    if(verify_condition(M, CONDITION)) {
        PC = ADDRESS;
    } else {
        PC = PC + BIS_JMP_SIZE;
    }
    CC = CC + BIS_JMP_CYCLES;
}

BIS_INSTRUCTION(call) {
    if(verify_condition(M, CONDITION)) {
        if(FR_E) {
            *DATA_MEMORY(SP - 0) = (uint8_t) (PC >> 24);
            *DATA_MEMORY(SP - 1) = (uint8_t) (PC >> 16);
            *DATA_MEMORY(SP - 2) = (uint8_t) (PC >> 8);
            *DATA_MEMORY(SP - 3) = (uint8_t) (PC >> 0);
        } else {
            *DATA_MEMORY(SP - 0) = (uint8_t) (PC >> 0);
            *DATA_MEMORY(SP - 1) = (uint8_t) (PC >> 8);
            *DATA_MEMORY(SP - 2) = (uint8_t) (PC >> 16);
            *DATA_MEMORY(SP - 3) = (uint8_t) (PC >> 24);
        }
        SP = SP - 4;
        PC = ADDRESS;
    } else {
        PC = PC + BIS_CALL_SIZE;
    }
    CC = CC + BIS_CALL_CYCLES;
}

BIS_INSTRUCTION(rjmp) {
    if(verify_condition(M, CONDITION)) {
        PC = PC + (OFFSET << 2);
    } else {
        PC = PC + BIS_RJMP_SIZE;
    }
    CC = CC + BIS_RJMP_CYCLES;
}

BIS_INSTRUCTION(rcall) {
    if(verify_condition(M, CONDITION)) {
        if(FR_E) {
            *DATA_MEMORY(SP - 0) = (uint8_t) (PC >> 24);
            *DATA_MEMORY(SP - 1) = (uint8_t) (PC >> 16);
            *DATA_MEMORY(SP - 2) = (uint8_t) (PC >> 8);
            *DATA_MEMORY(SP - 3) = (uint8_t) (PC >> 0);
        } else {
            *DATA_MEMORY(SP - 0) = (uint8_t) (PC >> 0);
            *DATA_MEMORY(SP - 1) = (uint8_t) (PC >> 8);
            *DATA_MEMORY(SP - 2) = (uint8_t) (PC >> 16);
            *DATA_MEMORY(SP - 3) = (uint8_t) (PC >> 24);
        }
        SP = SP - 4;
        PC = PC + (OFFSET << 2);
    } else {
        PC = PC + BIS_RCALL_SIZE;
    }
    CC = CC + BIS_RCALL_CYCLES;
}

BIS_INSTRUCTION(ijmp) {
    if(verify_condition(M, CONDITION)) {
        PC = RY + RZ;
    } else {
        PC = PC + BIS_IJMP_SIZE;
    }
    CC = CC + BIS_IJMP_CYCLES;
}

BIS_INSTRUCTION(icall) {
    if(verify_condition(M, CONDITION)) {
        if(FR_E) {
            *DATA_MEMORY(SP - 0) = (uint8_t) (PC >> 24);
            *DATA_MEMORY(SP - 1) = (uint8_t) (PC >> 16);
            *DATA_MEMORY(SP - 2) = (uint8_t) (PC >> 8);
            *DATA_MEMORY(SP - 3) = (uint8_t) (PC >> 0);
        } else {
            *DATA_MEMORY(SP - 0) = (uint8_t) (PC >> 0);
            *DATA_MEMORY(SP - 1) = (uint8_t) (PC >> 8);
            *DATA_MEMORY(SP - 2) = (uint8_t) (PC >> 16);
            *DATA_MEMORY(SP - 3) = (uint8_t) (PC >> 24);
        }
        SP = SP - 4;
        PC = RY + RZ;
    } else {
        PC = PC + BIS_ICALL_SIZE;
    }
    CC = CC + BIS_ICALL_CYCLES;
}

BIS_INSTRUCTION(ret) {
    (void) RX; /* Suppreses unused argument warning */

    PC = PC + BIS_RET_SIZE;
    CC = CC + BIS_RET_CYCLES;

    PC = 0;
    if(FR_E) {
        PC |= ((bis_register_t) *DATA_MEMORY(SP + 1)) << 24;
        PC |= ((bis_register_t) *DATA_MEMORY(SP + 2)) << 16;
        PC |= ((bis_register_t) *DATA_MEMORY(SP + 3)) << 8;
        PC |= ((bis_register_t) *DATA_MEMORY(SP + 4)) << 0;
    } else {
        PC |= ((bis_register_t) *DATA_MEMORY(SP + 1)) << 0;
        PC |= ((bis_register_t) *DATA_MEMORY(SP + 2)) << 8;
        PC |= ((bis_register_t) *DATA_MEMORY(SP + 3)) << 16;
        PC |= ((bis_register_t) *DATA_MEMORY(SP + 4)) << 24;
    }

    SP = SP + 4;
}

/*****************************************************************************
*                                   Misc                                     *
*****************************************************************************/

BIS_INSTRUCTION(movbit) {
    bis_register_t R = RX;

    MOV_BIT(R, J, RY, K);

    PC = PC + BIS_MOVBIT_SIZE;
    CC = CC + BIS_MOVBIT_CYCLES;

    RX = R;
}

BIS_INSTRUCTION(push) {
    PC = PC + BIS_PUSH_SIZE;
    CC = CC + BIS_PUSH_CYCLES;

    if(FR_E) {
        *DATA_MEMORY(SP - 0) = (uint8_t) (RX >> 24);
        *DATA_MEMORY(SP - 1) = (uint8_t) (RX >> 16);
        *DATA_MEMORY(SP - 2) = (uint8_t) (RX >> 8);
        *DATA_MEMORY(SP - 3) = (uint8_t) (RX >> 0);
    } else {
        *DATA_MEMORY(SP - 0) = (uint8_t) (RX >> 0);
        *DATA_MEMORY(SP - 1) = (uint8_t) (RX >> 8);
        *DATA_MEMORY(SP - 2) = (uint8_t) (RX >> 16);
        *DATA_MEMORY(SP - 3) = (uint8_t) (RX >> 24);
    }

    SP = SP - 4;
}

BIS_INSTRUCTION(pop) {
    PC = PC + BIS_POP_SIZE;
    CC = CC + BIS_POP_CYCLES;

    RX = 0;
    if(FR_E) {
        RX |= ((bis_register_t) *DATA_MEMORY(SP + 1)) << 24;
        RX |= ((bis_register_t) *DATA_MEMORY(SP + 2)) << 16;
        RX |= ((bis_register_t) *DATA_MEMORY(SP + 3)) << 8;
        RX |= ((bis_register_t) *DATA_MEMORY(SP + 4)) << 0;
    } else {
        RX |= ((bis_register_t) *DATA_MEMORY(SP + 1)) << 0;
        RX |= ((bis_register_t) *DATA_MEMORY(SP + 2)) << 8;
        RX |= ((bis_register_t) *DATA_MEMORY(SP + 3)) << 16;
        RX |= ((bis_register_t) *DATA_MEMORY(SP + 4)) << 24;
    }

    SP = SP + 4;
}

BIS_INSTRUCTION(swap) {
    bis_register_t TMP_RX = RX;
    bis_register_t TMP_RY = RY;

    PC = PC + BIS_SWAP_SIZE;
    CC = CC + BIS_SWAP_CYCLES;

    RX = TMP_RY;
    RY = TMP_RX;
}

BIS_INSTRUCTION(syscall) {
    PC = PC + BIS_SYSCALL_SIZE;
    CC = CC + BIS_SYSCALL_CYCLES;

    char *read_buffer[16];
    long l; unsigned long ul;

    switch (RX) {
        case 0x0U: /* Put Character */
            putchar((int) R0);
        break;

        case 0x1U: /* Get Character */
            R0 = (bis_register_t) getchar();
        break;

        case 0x2U: /* Put String */
            fputs((const char *) DATA_MEMORY(R0), stdout);
        break;

        case 0x3U: /* Get String */
            fgets((char *) DATA_MEMORY(R0), (int) R1, stdin);
            R1 = (bis_register_t) strlen((const char *) DATA_MEMORY(R0));
        break;

        case 0x4U: /* Put Signed Decimal*/
            printf("%"PRIi32, R0);
        break;

        case 0x5U: /* Put Unsigned Decimal*/
            printf("%"PRIu32, R0);
        break;

        case 0x6U: /* Put Signed Hexadecimal*/
            printf("%s0x%08X", GET_BIT(R0, 31) ? "-" : "", -R0);
        break;

        case 0x7U: /* Put Unsigned Hexadecimal*/
            printf("0x%08X", R0);
        break;

        case 0x8U: /* Get Signed Integer */
            fgets((char *) read_buffer, 16, stdin);
            l = strtol((char *) read_buffer, NULL, 0);
            if(errno || l > INT32_MAX || l < INT32_MIN) {
                R0 = 0;
                SET_BIT(FR, BIS_FR_O);
            } else {
                R0 = (bis_register_t) l;
                CLEAR_BIT(FR, BIS_FR_O);
            }
        break;

        case 0x9U: /* Get Unsigned Integer */
            fgets((char *) read_buffer, 16, stdin);
            ul = strtoul((char *) read_buffer, NULL, 0);
            if(errno || ul > UINT32_MAX) {
                R0 = 0;
                SET_BIT(FR, BIS_FR_O);
            } else {
                R0 = (bis_register_t) ul;
                CLEAR_BIT(FR, BIS_FR_O);
            }
        break;

        case 0xFFFFFFFFU:
            M->state = BIS_HALTED_STATE;
        break;

        default:
            ERROR_MSG("Unexpected syscall number: 0x%08X.", RX);
    }
}

BIS_INSTRUCTION(nop) {
    (void) RX; /* Suppreses unused argument warning */
    PC = PC + BIS_NOP_SIZE;
    CC = CC + BIS_NOP_CYCLES;
}

BIS_INSTRUCTION(break) {
    (void) RX; /* Suppreses unused argument warning */
    /* TODO Allow some configurable options here.
    I.e.: Print current state, or start a command line "debuger" */
    M->state = BIS_BREAK_STATE;
    PC = PC + BIS_BREAK_SIZE;
    CC = CC + BIS_BREAK_CYCLES;
}

/* Not on the ISA but implemented for convenience on the emulator. */
BIS_INSTRUCTION(undefined) {
    (void) RX; /* Suppreses unused argument warning */

    ERROR_MSG("Treating undefined opcode \'0x%02X\' as NOP!", IRH >> 25);
    bis_print_registers(M, 1LLU << BIS_PC_REG | 1LLU << BIS_IRL_REG |
                           1LLU << BIS_IRH_REG | 1LLU << BIS_CC_REG, stderr);
   PC = PC + BIS_UNDEFINED_SIZE;
   CC = CC + BIS_UNDEFINED_CYCLES;
}
