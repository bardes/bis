#include "bis.h"
#include "utils.h"

#include <stdio.h>

int main(int argc, char const* argv[])
{
    FAIL_MSG(argc != 2, "Bad arguments.");
    FILE *input = fopen(argv[1], "r");
    FAIL(input == NULL);

    bis_memory_layout_t layout = {
        .program_memory_size = 1 * 1024,

        .fast_memory_size = 1 * 1024,
        .fast_memory_offset = 0,

        .data_memory_size = 8 * 1024,
        .data_memory_offset = 0x00000400,

        .video_memory_size = 32 * 1024,
        .video_memory_offset = 0x00002400
    };

    bis_machine_state_t *M = bis_new_machine_state(&layout);

    uint32_t section_length;
    fread(&section_length, 4, 1, input);
    fread(M->memory_state.program_memory.bytes, 1, (size_t) section_length, input);
    fread(&section_length, 4, 1, input);
    fread(M->memory_state.data_memory.bytes, 1, (size_t) section_length, input);
    fclose(input);

    while(M->state == BIS_RUNING_STATE) {
        int err; if((err = bis_exec_machine(M, 1000000)) != 0) {
            ERROR_MSG("Execution failed with code %d.", err);
            bis_print_registers(M, 1ULL << BIS_PC_REG
                                 | 1ULL << BIS_CC_REG, stderr);
            break;
        }
    }

    bis_free_machine_state(M);
    return 0;

fail:
    return 1;
}
