#ifndef BIS_INSTRUCTION_H
#define BIS_INSTRUCTION_H

#include "bis.h"

#define BIS_INSTRUCTION(name) \
    void bis_ ## name ## _inst(bis_machine_state_t *M, \
    const bis_instruction_args_t *args)

#include "instruction_prototypes.gen"

/* Exrtra instruction used whenever an undefined instruction is executed */
#define BIS_UNDEFINED_SIZE 4U
#define BIS_UNDEFINED_CYCLES 1U
BIS_INSTRUCTION(undefined);

#endif
