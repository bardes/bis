#include "bis.h"

#include <stdlib.h>

#include "utils.h"

const char * const BIS_REGISTER_NAMES[BIS_NREGS] = {
    /* GPRs */
     "r0",  "r1",  "r2",  "r3",  "r4",  "r5",  "r6",  "r7",
     "r8",  "r9", "r10", "r11", "r12", "r13", "r14", "r15",
    "r16", "r17", "r18", "r19", "r20", "r21", "r22", "r23",
    "r24", "r25", "r26", "r27", "r28", "r29", "r30", "r31",

    /* SPRs */
    "mar", "mdr", "pc", "irh", "irl", "sp", "fp", "slr",
     "fr",  "zr", "im",  "cc",

    /* Extra */
    "ext3", "ext2", "ext1", "ext0"
};


bis_machine_state_t *bis_new_machine_state(const bis_memory_layout_t *l)
{
    bis_machine_state_t *M = NULL;

    M = calloc(1, sizeof(bis_machine_state_t));
    FAIL(M == NULL);

    int err = bis_realloc_machine_state(M, l);
    FAIL_MSG(err == -1, "Could not allocate machine state!");
    FAIL_MSG(err == -2, "Bad memory layout!");//TODO

    return M;

fail:
    free(M);
    return NULL;
}

/**
 * FIXME: De fato realocar, sem perder os dados.
 * Permitir trocar apenas offsets
 * Verificar regiões sobrepostas?
 */
int bis_realloc_machine_state(
        bis_machine_state_t *M, const bis_memory_layout_t *l)
{
    bis_memory_state_t ms = {
        .program_memory.bytes = NULL,
        .data_memory.bytes    = NULL,
        .fast_memory.bytes    = NULL,
        .video_memory.bytes   = NULL,
    };

    /* Allocates each new memory section */
    ms.program_memory.bytes = calloc(1, l->program_memory_size);
    ms.data_memory.bytes    = calloc(1, l->data_memory_size);
    ms.fast_memory.bytes    = calloc(1, l->fast_memory_size);
    ms.video_memory.bytes   = calloc(1, l->video_memory_size);

    /* Fails if any of them are NULL */
    FAIL(ms.program_memory.bytes == NULL &&
            ms.data_memory.bytes == NULL &&
            ms.fast_memory.bytes == NULL &&
            ms.video_memory.bytes == NULL);

    /* Frees the old sections */
    free(M->memory_state.program_memory.bytes);
    free(M->memory_state.data_memory.bytes);
    free(M->memory_state.fast_memory.bytes);
    free(M->memory_state.video_memory.bytes);

    /* Finally assigns the new sections and layout */
    M->memory_state = ms;
    M->memory_layout = *l;
    M->state = BIS_RUNING_STATE; /* FIXME? Lugar melhor pra fazer isso? */
    return 0;

fail:
    free(ms.program_memory.bytes);
    free(ms.data_memory.bytes);
    free(ms.fast_memory.bytes);
    free(ms.video_memory.bytes);
    return -1;
}

int bis_reset_machine_state(bis_machine_state_t *M)
{
    return -1;
}

int bis_copy_machine_state(
        const bis_machine_state_t *src, bis_machine_state_t **dest)
{
    return -1;
}

void bis_free_machine_state(bis_machine_state_t *M)
{
    if(M) {
        free(M->memory_state.program_memory.bytes);
        free(M->memory_state.data_memory.bytes);
        free(M->memory_state.fast_memory.bytes);
        free(M->memory_state.video_memory.bytes);
        free(M);
    }
}

void bis_print_registers(
        const bis_machine_state_t *M, uint64_t registers, FILE *output)
{
    size_t col = 0;
    static const size_t n_cols = 5;

    for(bis_register_id_t reg = 0; reg < BIS_NREGS; ++reg) {
        if(registers & (1LLU << reg)) {
            if(col) fputs("    ", output);
            fprintf(output, "%4s: 0x%08X",
                    BIS_REGISTER_NAMES[reg], M->registers[reg]);

            col = (col + 1) % n_cols;
            if(col == 0)
                fputc('\n', output);
        }
    }

    if(col) fputs("\n\n", output);
}
