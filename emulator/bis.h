#ifndef BIS_BIS_H
#define BIS_BIS_H

#include <stdint.h>
#include <stdio.h>

#include "registers.gen"
#include "instructions.gen"
#include "conditions.gen"

typedef uint32_t bis_register_t;
typedef uint8_t bis_register_id_t;
typedef uint8_t bis_condition_t;
typedef uint8_t bis_opcode_t;

typedef enum {
    BIS_HALTED_STATE,
    BIS_RUNING_STATE,
    BIS_BREAK_STATE,
} bis_state_t;

typedef struct {
    uint32_t program_memory_size;

    uint32_t data_memory_size;
    uint32_t data_memory_offset;

    uint32_t video_memory_size;
    uint32_t video_memory_offset;

    uint32_t fast_memory_size;
    uint32_t fast_memory_offset;
} bis_memory_layout_t;

typedef struct {
    union {
        uint8_t *bytes;
        uint32_t *words;
    } program_memory;

    union {
        uint8_t *bytes;
        uint32_t *words;
    } data_memory;

    union {
        uint8_t *bytes;
        uint32_t *words;
    } video_memory;

    union {
        uint8_t *bytes;
        uint32_t *words;
    } fast_memory;
} bis_memory_state_t;

typedef struct {
    bis_register_t registers[BIS_NREGS];
    bis_memory_state_t memory_state;
    bis_memory_layout_t memory_layout;
    bis_state_t state;
} bis_machine_state_t;

typedef struct {
    union {
        bis_register_id_t rx;
        bis_condition_t condition;
    };
    bis_register_id_t ry, rz, rw;
    union {
        bis_register_t immediate;
        bis_register_t address;
        bis_register_t offset;
        struct {uint8_t j; uint8_t k;};
    };
} bis_instruction_args_t;

typedef void (*bis_instruction_t)
    (bis_machine_state_t *, const bis_instruction_args_t *);

bis_machine_state_t *bis_new_machine_state(const bis_memory_layout_t *l);

int bis_realloc_machine_state
(bis_machine_state_t *M, const bis_memory_layout_t *l);

int bis_reset_machine_state(bis_machine_state_t *M);

/* If dest is NULL allocates a new state for the copy. */
int bis_copy_machine_state
(const bis_machine_state_t *src, bis_machine_state_t **dest);

void bis_free_machine_state(bis_machine_state_t *M);

void bis_print_registers
(const bis_machine_state_t *M, uint64_t registers, FILE *output);

int bis_exec_machine(bis_machine_state_t *M, size_t cycles);

extern const char * const BIS_REGISTER_NAMES[BIS_NREGS];

#endif
