#ifndef BIS_UTILS_H_
#define BIS_UTILS_H_

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define ERROR(...) \
    do{ \
        int __utils_err = errno; \
        fprintf(stderr,"[ERROR]      %s:%04u: ", __FILE__, __LINE__); \
        fprintf(stderr, __VA_ARGS__); \
        fprintf(stderr, ": %s\n", strerror(__utils_err)); \
        errno = __utils_err; \
    } while(0)
#define ERROR_MSG(...) \
    do{ \
        int __utils_err = errno; \
        fprintf(stderr,"[ERROR]      %s:%04u: ", __FILE__, __LINE__); \
        fprintf(stderr, __VA_ARGS__); \
        fputc('\n', stderr); \
        errno = __utils_err; \
    } while(0)

#define WARNING(...) \
    do{ \
        int __utils_err = errno; \
        fprintf(stderr,"[WARNING]    %s:%04u: ", __FILE__, __LINE__); \
        fprintf(stderr, __VA_ARGS__); \
        fputc('\n', stderr); \
        errno = __utils_err; \
    } while(0)

#define FAIL_MSG(x, ...) \
    do{ \
        int __utils_err = errno; \
        if (x) { \
            fprintf(stderr,"[FAIL]       %s:%04u: ", __FILE__, __LINE__); \
            fprintf(stderr, __VA_ARGS__); \
            fputc('\n', stderr); \
            errno = __utils_err; \
            goto fail; \
        } \
    } while(0)

#define FAIL(x) \
    do { \
        int __utils_err = errno; \
        if (x) { \
            fprintf(stderr, "[FAIL]       %s:%04u: %s\n", \
                    __FILE__, __LINE__, strerror(errno)); \
            errno = __utils_err; \
            goto fail; \
        } \
    } while(0) \

#define FATAL_MSG(x, ext, ...) \
    do { \
        if (x) { \
            fprintf(stderr,"[FATAL]      %s:%04u: ", __FILE__, __LINE__); \
            fprintf(stderr, __VA_ARGS__); \
            fputc('\n', stderr); \
            exit(ext); \
        } \
    } while(0) \

#define FATAL(x, ext) \
    do { \
        if (x) \
        { \
            fprintf(stderr, "[FATAL]      %s:%04u: %s\n", \
                    __FILE__, __LINE__, strerror(errno)); \
            exit(ext); \
        } \
    } while(0) \

#ifdef DEBUG
#define DMSG(msg, ...) \
    do { \
        int __utils_err = errno; \
        fprintf(stderr, "[DEBUG]      %s:%04u: ", __FILE__, __LINE__); \
        fprintf(stderr, msg "\n", __VA_ARGS__); \
        errno = __utils_err; \
    } while(0)
#else
#define DMSG(...) do{}while(0)
#endif

#define SET_BIT(x, y) do {(x) |= 1LLU << (y);} while(0)
#define CLEAR_BIT(x, y) do {(x) &= ~(1LLU << (y));} while(0)
#define TOGGLE_BIT(x, y) do {(x) ^= 1LLU << (y);} while(0)
#define GET_BIT(x, y) (((x) & (1LLU << (y))) >> (y))

#define MOV_BIT(x, n, y, m) do {\
    CLEAR_BIT((x), (n)); (x) |= GET_BIT((y), (m)) << (n);} while(0)

#endif /* BIS_UTILS_H_ */
