print(("Start clock: %s"):format(os.clock()))

local ram = {
    42,
    44,
    8735,
    37982,
    44372
}

local ops = {
    function(rx, ry)
        return rx + ry
    end;

    function(rx, ry)
        return rx * ry
    end;

    function(rx, n, ry, m)
        ram[n % #ram + 1] = m
        return 1/(rx*ry+rx)+m and (m or rx) and (rx * rx)
    end;

    function(rx)
        return ram[rx % #ram + 1]
    end;

    function(rx, n)
        ram[rx % #ram + 1] = n
    end;
}

local n_ops = {
    {1000,   "1K"},
    {10000,  "10K"},
    {50000,  "50K"},
    {100000, "100K"},
    {500000, "500K"},
    {1e6,    "1M"},
    {2.5e6,  "2.5M"},
    {5e6,    "5M"},
    {10e6,   "10M"},
    {50e6,   "50M"},
    {100e6,  "100M"},
}

for _, n in ipairs(n_ops) do
    local start_time = os.clock()
    for i = 1, n[1] do
        ops[math.random(1, 5)](math.random(0,31), math.random(0,31), math.random(0,31), math.random(0,31))
    end
    local elapsed_time = os.clock() - start_time
    print(("%8s ops in %.3fs: %f MIPS"):format(n[2], elapsed_time, n[1]/(elapsed_time*1e6)))
end

print(("End clock: %f"):format(os.clock()))
