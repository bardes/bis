#include <stdio.h>
#include <stdlib.h>
#include <time.h>

unsigned int reg[32];

void add(unsigned int *reg, unsigned int rx, unsigned int ry, unsigned int rz)
{
    reg[rx] = reg[ry] + reg[rz];
}

void mul(unsigned int *reg, unsigned int rx, unsigned int ry, unsigned int rz)
{
    reg[rx] = reg[ry] * reg[rz];
}

void mdiv(unsigned int *reg, unsigned int rx, unsigned int ry, unsigned int rz)
{
    if(reg[rz]) {
        unsigned q, r;
        q = reg[ry] / reg[rz];
        r = reg[ry] % reg[rz];

        reg[rx] = q;
        reg[(rx+1) % 32] = r;
    }
}

void wtf(unsigned int *reg, unsigned int rx, unsigned int ry, unsigned int rz)
{
    reg[rx] = reg[ry] * reg[rz] + random() % 256;
    if(reg[rx] > 0x80000000) {
        reg[rz] = reg[rz]++;
    } else {
        reg[rz] = 1.234 * reg[rx] + (reg[ry] ^ reg[rz]);
    }
}

typedef void (*op)(unsigned int*, unsigned int, unsigned int, unsigned int);

op ops[] = {
    add,
    mul,
    mdiv,
    wtf
};


int main(int argc, char const* argv[])
{
    srandom(0xA87A95B3);

    size_t n[] = {
        100,        //.1k
        1000,       // 1k
        10000,      // 10k
        100000,     // 100k
        500000,     // 500k
        1000000,    // 1M
        2000000,    // 2M
        5000000,    // 5M
        10000000,   // 10M
        50000000,   // 50M
        100000000   // 100M
    };

    size_t len = sizeof(n)/sizeof(n[0]);

    printf("Hello");
    for (size_t i = 0; i < 32; i++) {
        reg[i] = random();
    }

    for (size_t i = 0; i < len; i++) {
        clock_t start_clock = clock();
        for(size_t j = 0; j < n[i]; ++j) {
            ops[random() % 4](reg, random() % 32, random() % 32, random() % 32);
        }
        clock_t elapsed_clock = clock() - start_clock;
        double elapsed_time = elapsed_clock / (double) CLOCKS_PER_SEC;
        printf("%8zu ops in %.3lfs: %.3lf MIPS\n", n[i], elapsed_time, n[i]/(elapsed_time * 1e6));
    }
    return 0;
}
