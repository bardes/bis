#include <stdio.h>
#include <stdint.h>

typedef int (*teste)(int, int);
int g(int x, int y) { return x + y; }
teste f = g;

int main()
{
    uint64_t R;
    uint32_t A, B;
    A = B = 0xFFFFFFFF;
    R = (uint64_t)A + B;

    printf("0x%08llX = 0x%04X + 0x%04X\n", R, A, B);

    return 5 - f(2, 3);
}
