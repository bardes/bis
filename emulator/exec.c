#include <arpa/inet.h>

#include "bis.h"
#include "instructions.h"
#include "utils.h"

/* TODO: Decode all instructions at onece, before execution? */

typedef struct {
    const char *mnemonic;
    bis_instruction_t exec;
    size_t size;
    bis_instruction_format_t format;
} bis_instruction_info_t;

static const bis_instruction_info_t BIS_INSTRUCTION_TABLE[BIS_NINSTS] = {
    #include "instruction_table.gen"
};

static bis_instruction_args_t decode_r(const bis_register_t *inst)
{
    bis_instruction_args_t args;
    args.rw = (inst[0] >>  1) & 0x3FU;
    args.rz = (inst[0] >>  7) & 0x3FU;
    args.ry = (inst[0] >> 13) & 0x3FU;
    args.rx = (inst[0] >> 19) & 0x3FU;

    return args;
}

static bis_instruction_args_t decode_i(const bis_register_t *inst)
{
    bis_instruction_args_t args = decode_r(inst);
    args.immediate = inst[1];
    return args;
}

static bis_instruction_args_t decode_j(const bis_register_t *inst)
{
    bis_instruction_args_t args;
    args.condition = (inst[0] >> 20) & 0x1F;
    args.address = inst[1];
    return args;
}

static bis_instruction_args_t decode_rj(const bis_register_t *inst)
{
    bis_instruction_args_t args;

    /* Sign extend */
    args.offset  = GET_BIT(inst[0], 19) ? 0xFFC00000U : 0x0U;

    /* Word align */
    args.offset |= (inst[0] & 0x000FFFFFU);

    args.condition = (inst[0] >> 20) & 0x1F;
    return args;
}

static bis_instruction_args_t decode_ij(const bis_register_t *inst)
{
    bis_instruction_args_t args;
    args.ry = (inst[0] >> 13) & 0x3FU;
    args.rx = (inst[0] >> 19) & 0x3FU;
    args.condition = (inst[0] >> 20) & 0x1F;
    return args;
}

static bis_instruction_args_t decode_m(const bis_register_t *inst)
{
    bis_instruction_args_t args;
    args.k  = (inst[0] >>  3) & 0x1FU;
    args.j  = (inst[0] >>  8) & 0x1FU;
    args.ry = (inst[0] >> 13) & 0x3FU;
    args.rx = (inst[0] >> 19) & 0x3FU;
    args.condition = (inst[0] >> 20) & 0x1F;
    return args;
}

static bis_opcode_t
decode_instruction(const bis_register_t *inst, bis_instruction_args_t *args)
{
    bis_opcode_t opcode = (bis_opcode_t) (*inst >> 25);
    switch (BIS_INSTRUCTION_TABLE[opcode].format) {
        case BIS_R0_FORMAT:
        case BIS_R1_FORMAT:
        case BIS_R2_FORMAT:
        case BIS_R3_FORMAT:
        case BIS_R4_FORMAT:
            *args = decode_r(inst);
            break;

        case BIS_I1_FORMAT:
        case BIS_I2_FORMAT:
        case BIS_I3_FORMAT:
        case BIS_I4_FORMAT:
        case BIS_I5_FORMAT:
            *args = decode_i(inst);
            break;

        case BIS_J_FORMAT:
            *args = decode_j(inst);
            break;

        case BIS_RJ_FORMAT:
            *args = decode_rj(inst);
            break;

        case BIS_IJ_FORMAT:
            *args = decode_ij(inst);
            break;

        case BIS_M_FORMAT:
            *args = decode_m(inst);
            break;

        case BIS_UNKNOWN_FORMAT:
            ERROR_MSG("Bad opcode 0x%2hhX! Will be treated as NOP instead.",
            opcode);
    }

    return opcode;
}

/* Returns positive on warning and negative on error */
int bis_exec_machine(bis_machine_state_t *M, size_t instructions)
{
    for(size_t n = 0; n < instructions && M->state == BIS_RUNING_STATE; ++n) {
        bis_opcode_t opcode; bis_instruction_args_t args;
#ifdef BIS_SAFETY_CHECK
        const bis_register_t irh_pos = (M->registers[BIS_PC_REG] + 0)
        % M->memory_layout.program_memory_size;
        const bis_register_t irl_pos = (M->registers[BIS_PC_REG] + 4)
        % M->memory_layout.program_memory_size;

        /* Fetch */
        M->registers[BIS_IRH_REG] =
        M->memory_state.program_memory.words[irh_pos / 4];
        M->registers[BIS_IRL_REG] =
        M->memory_state.program_memory.words[irl_pos / 4];
        opcode = decode_instruction(M->registers + BIS_IRH_REG, &args);

        if(M->registers[BIS_PC_REG] % 4) {
            WARNING("PC is not word aligned! The last two bits will be truncated.");
            return 1;
        }

        if(M->registers[BIS_PC_REG] >= M->memory_layout.program_memory_size) {
            WARNING("PC is out of bounds, wraping around!");
            return 1;
        }

        if(M->registers[BIS_PC_REG] == M->memory_layout.program_memory_size
        && BIS_INSTRUCTION_TABLE[opcode].size > 4) {
            ERROR_MSG("Immediate value is out of bounds!");
            bis_print_registers(M, 1ULL << BIS_PC_REG
                                 | 1ULL << BIS_CC_REG, stderr);
            return -1;
        }
#else
        M->registers[BIS_IRH_REG] =
        M->memory_state.program_memory.words[M->registers[BIS_PC_REG] / 4];
        M->registers[BIS_IRL_REG] =
        M->memory_state.program_memory.words[M->registers[BIS_PC_REG] / 4 + 1];
        opcode = decode_instruction(M->registers + BIS_IRH_REG, &args);
#endif

        /* Execute */
        M->registers[BIS_ZR_REG] = 0; /* zr must always be zero */
        BIS_INSTRUCTION_TABLE[opcode].exec(M, &args);

        /* Interrupts */
        /* TODO */

        /* Must keep the zero register zeroed */
        M->registers[BIS_ZR_REG] = 0U;

        /* Check for stack overflow */
#ifdef BIS_SAFETY_CHECK
        if(M->registers[BIS_SP_REG] < M->registers[BIS_SLR_REG]) {
            ERROR_MSG("Stack overflow! SP is over the SLR limit.");
            bis_print_registers(M, 1ULL << BIS_PC_REG | 1ULL << BIS_CC_REG
                                 | 1ULL << BIS_SP_REG | 1ULL << BIS_SLR_REG,
                                 stderr);
            return -1;
        }
#endif
    }
    return 0;
}
