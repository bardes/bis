#!/usr/bin/env lua5.3

-- Adds the file path to the package search path
local file_path = debug.getinfo(1, "S").source:match("@(.*/)") or './'
package.path = table.concat {
    file_path, '../bis/?.lua;';
    file_path, '?.lua;';
    package.path
}

local utils = require 'utils'
local parser = require 'parser'
local codegen = require 'codegen'

local argparse = require 'argparse'

local argparser = argparse('bis-assembler', "The BIS Assembler")
argparser:argument('input', "Input file. (use '-' for stdin)")
argparser:mutex(
    argparser:option('-o --output', 'Output file.'),
    argparser:flag('--stdout', 'Output to stdout.')
)
argparser:option('-f --format', 'Output format.\nFormats: bis, elf', 'bis')
argparser:option('-v --verbosity', 'Assembler verbosity level.\nLevels: [q]uiet, [n]ormal, [v]erbose, [a]ll', 'normal')
argparser:mutex(
    argparser:flag('-be --big-endian', 'Output code in big-endian format.'),
    argparser:flag('-le --little-endian', 'Output code in little-endian format.'),
    argparser:flag('-ne --native', 'Output code with the same endianness as the host machine. (default)')
)

local args = argparser:parse()

-- Checking the output file format
if args.format:lower() == 'elf' then
    io.stderr:write('Error: ELF output format not suported yet :(\n')
    os.exit(1)
elseif args.format:lower() ~= 'bis' then
    io.stderr:write(('Error: %q is not a valid output format\n')
    :format(args.format))
    os.exit(1)
end

-- Sets the verbosity level
local options = {}
options.verbosity = args.verbosity:lower()

-- Sets the endiannes
if args.big_endian then options.endianness = '>'
elseif args.little_endian then options.endianness = '<'
else options.endianness = '=' end

-- Read input file
local infile
if args.input == '-' then
    infile = io.stdin
else
    local err; infile, err = io.open(args.input, 'r')
    if err then print('Error: ' .. err) os.exit(1) end
end
local input = infile:read('*all')
infile:close()

-- Parses the input
local parsed = parser:match(input)
if parsed == nil then
    io.stderr:write('Parser error.\n')
    os.exit(2)
end

-- And generates the code
local sections, symbol_table = codegen(parsed, options)
if sections == nil then
    io.stderr:write('Assembly failed.\n')
    os.exit(2)
end

-- Opens the output file
local output
if args.stdout then
    if args.output ~= nil then
        io.stderr:write(('>>> %q\n'):format(args.output))
        io.stderr:write('Warning: --stdout overrides --output.\n')
    end
    output = io.stdout
else
    -- If ther's no output file specified we must choose one
    if args.output == nil or args.output == '' then
        -- Outputs to stdout if reading from stdin
        if args.input == '-' then args.output = io.stdout

        -- Or creates a file with a '.out' suffix based on the input
        else args.output = args.input .. '.out' end
    end

    -- Opens the file
    local err; output, err = io.open(args.output, 'w+b')
    if err then io.stderr:write(('Error: %s\n'):format(err)) os.exit(1) end
end

-- Writes the output
if args.format == 'bis' then
    if sections['.text'] == nil or sections['.text'] == '' then
        io.stderr:write('Warning: .text section is empty. ' ..
        'Did you forget the .text directive?\n')
        sections['.text'] = ''
    end
    output:write(string.pack(options.endianness .. 's4s4', sections['.text'],
    sections['.data'] or ''))
elseif args.format == 'elf' then
    -- TODO implement elf support
end

output:close()
