
local utils = require 'utils'
local arch = require 'arch'

local lpeg = require 'lpeglabel'

-- Some lpeg shortcuts
local P = lpeg.P; local S = lpeg.S; local R = lpeg.R; local C = lpeg.C;
local Cc = lpeg.Cc; local Ct = lpeg.Ct; local Cg = lpeg.Cg; local Cp = lpeg.Cp;
local V = lpeg.V; local T = lpeg.T; local Rec = lpeg.Rec; local Cs = lpeg.Cs

-- This table defines the possible errors and their messages
local error_table = {
    [0] = {name = 'unknown', message = 'Unknown error.'},

    {name = 'bad_register', message = 'Bad register name.'},
    {name = 'bad_mnemonic', message = 'Bad instruction mnemonic.'},
}

-- Reindexes the error table by name for ease of use with grammars
local errors = utils.reindex(error_table, 'name', 'id')

-- Some useful definitions --
-- Always fails
local fail = P(1) - P(1)

-- End of file
local eof = P(-1)

-- End of line
local eol = P'\r\n' + P'\n'

-- Binary digits
local bin = S'01'

-- Decimal digits
local dec = R'09'

-- Hexadecimal digits
local hex = dec + R'AF' + R'af'

-- Alphabetic chars (lower case)
local alpha = R('az', 'AZ')

-- Alphanumeric chars (lower case)
local alnum = alpha + dec

-- White space
local sp = S' \t'

-- Instruction argument separator
local sep = P','

-- Directive prefix Ccaracter
local directive_prefix = P'.'

-- Label suffix character
local label_suffix = P':'

-- Comment prefix charater
local comment_prefix = ';'

-- Generic id used for labels and directives
local id = C((alpha + P'_') * (alnum + P'_')^0)

-- Optional space before 'p'
local function osp(p)
    return sp^0 * p
end

-- Special purpose registers
local SPR = T(errors.bad_register.id)
for _, mnemonic, reg in utils.sorted_pairs(arch.registers.spr.byMnemonic) do
    SPR = mnemonic * (-alnum) * Cc(reg.id) + SPR -- Adds all the SPRs
end

-- General purpose registers (r0-r31)
local GPR = 'r' * C('3' * S'01' + S'12' * R'09' + R'09')/tonumber

-- Matches any register and captures the register id
local REG = osp(GPR + SPR)

-- Some highly used patterns
local SEP = osp(sep)
local RX = Cg(REG, 'rx')
local RY = Cg(REG, 'ry')
local RZ = Cg(REG, 'rz')
local RW = Cg(REG, 'rw')


-- Generates a table of patterns that matches the mnemonics
-- of each instruction format
local MNM = {}
for format, _ in pairs(arch.instructions.formats) do
    MNM[format] = fail
end
for _, mnemonic, inst in utils.sorted_pairs(arch.instructions.byMnemonic) do
    if type(MNM[inst.format]) ~= 'nil' then
        MNM[inst.format] =
            mnemonic * Cg(Cc(inst.opcode), 'opcode') + MNM[inst.format]
    else
        io.stderr:write(("Instruction %q has an unknown format: %q\n")
        :format(mnemonic, tostring(inst.format)))
    end
end
for format, pattern in pairs(MNM) do
    MNM[format] = osp(pattern)
end

-- Adds conditions mnemonics to the table too
MNM.COND = fail
for _, mnemonic, condition in utils.sorted_pairs(arch.conditions.byMnemonic) do
    MNM.COND = mnemonic * Cc(condition.code) + MNM.COND;
end
MNM.COND = osp(MNM.COND)

local function typ(name)
    return Cg(Cc(name), 'type')
end

local function twos_complement(neg, n)
    if neg then n = (1 << 32) - n end
    return n % (1 << 32)
end

return P{ 'FILE';
    -- Initial rule, should match the whole translation unit
    FILE    = Ct((V'LINE' * osp(eol))^0 * V'LINE'^-1) * osp(eof),

    -- A line of input from the translation unit
    LINE    = V'LBL_DEF'^-1 * (V'INST' + V'DIRECTIVE')^-1 * V'COMM'^-1,

    -- Assembly directive
    DIRECTIVE = Ct(typ'directive' * V'DNAME' * (sp * V'DARGS')^-1),

    -- Directive name
    DNAME   = osp(directive_prefix) * Cg(V'ID', 'name'),

    -- Directive sequence of arguments
    DARGS   = Cg(Ct(V'DARG' * (SEP * V'DARG')^0), 'args'),

    -- Single directive argument
    DARG    = V'SYM' + V'NUM' + V'STR',

    -- Any single instruction
    INST    = Ct(typ'instruction' * (V'INST_R' + V'INST_I' + V'INST_J'
            + V'INST_RJ' + V'INST_IJ' + V'INST_M')),

    -- Register (any number of args)
    INST_R  = V'INST_R0' + V'INST_R1' + V'INST_R2' + V'INST_R3' + V'INST_R4',

    -- Immediate (any number of args)
    INST_I  = V'INST_I1' + V'INST_I2' + V'INST_I3' + V'INST_I4' + V'INST_I5',

    -- Absolute jump/call
    INST_J  = MNM.J  * sp * V'COND' * SEP * V'ADDR',

    -- Relative jump/call
    INST_RJ = MNM.RJ * V'COND' * SEP * V'OFFSET',

    -- Indirect jump/call
    INST_IJ = MNM.IJ * sp * V'COND' * SEP * RY * SEP * RZ,

    -- Movbit
    INST_M  = MNM.M * sp * RX * SEP * RY * SEP * osp(Cg(V'NUM', 'j'))
                                         * SEP * osp(Cg(V'NUM', 'k')),

    -- Intruction without any arguments (0 registers)
    INST_R0 = MNM.R0,

    -- Single register argument
    INST_R1 = MNM.R1 * sp * RX,

    -- Two register arguments
    INST_R2 = MNM.R2 * sp * RX * SEP * RY,

    -- Three register arguments
    INST_R3 = MNM.R3 * sp * RX * SEP * RY * SEP * RZ,

    -- Four register arguments
    INST_R4 = MNM.R4 * sp * RX * SEP * RY * SEP * RZ * SEP * RW,


    -- One immediate argument
    INST_I1 = MNM.I1 * sp * V'IMM',

    -- Two arguments, one register and one immediate
    INST_I2 = MNM.I2 * sp * RX * SEP * V'IMM',

    -- Three arguments, two registers and one immediate
    INST_I3 = MNM.I3 * sp * RX * SEP * RY * SEP * V'IMM',

    -- Four arguments, three registers and one immediate
    INST_I4 = MNM.I4 * sp * RX * SEP * RY * SEP * RZ * SEP * V'IMM',

    -- Five arguments, four registers and one immediate
    INST_I5 = MNM.I5 * sp * RX * SEP * RY * SEP * RZ * SEP * RW * SEP * V'IMM',


    -- Numbers captured as specific arg
    IMM     = Cg(V'NUM' + V'SYM', 'immediate'),
    ADDR    = Cg(V'NUM' + V'SYM', 'address'),
    OFFSET  = Cg(V'NUM' + V'SYM', 'offset'),

    -- Jump condition literal
    COND    = Cg(V'NUM' + MNM.COND, 'condition'),

    -- Numbers in base 2, 10 or 16
    NUM     = (V'UMINUS' * (V'BIN' + V'HEX' + V'DEC')) / twos_complement,
    UMINUS  = osp(P'-' * Cc(true) + Cc(false)),
    BIN     = osp('0b' * (C(bin^1) * Cc(2)) / tonumber),
    HEX     = osp('0x' * (C(hex^1) * Cc(16)) / tonumber),
    DEC     = osp(C(dec^1) / tonumber),

    -- Generic ID
    ID      = osp(id),

    -- A label definition
    LBL_DEF = Ct(typ'label' * osp(Cg(id, 'name')) * label_suffix),

    -- A symbol from the sybol table
    SYM     = osp(id),

    -- Quoted string
    STR     = osp('"' * Cs(((P'\\n'/'\n') + (P(1) - '"'))^0) * '"'),

    -- A comment
    COMM    = osp(comment_prefix) * (P(1) - '\n')^0
}
