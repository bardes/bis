-- TODO Check for numeric values out of bounds
local utils = require "utils"

local gen = {}

function gen.R0(inst, endianness)
    return string.pack(endianness .. 'I4', inst.opcode << 25 | 0x424953)
end

function gen.R1(inst, endianness)
    return string.pack(endianness .. 'I4', inst.opcode << 25 | inst.rx << 19
    | 0x5042)
end

function gen.R2(inst, endianness)
    return string.pack(endianness .. 'I4', inst.opcode << 25 | inst.rx << 19
    | inst.ry << 13 | 0x4D)
end

function gen.R3(inst, endianness)
    return string.pack(endianness .. 'I4', inst.opcode << 25 | inst.rx << 19
    | inst.ry << 13 | inst.rz << 7)
end

function gen.R4(inst, endianness)
    return string.pack(endianness .. 'I4', inst.opcode << 25 | inst.rx << 19
    | inst.ry << 13 | inst.rz << 7 | inst.rw << 1)
end

function gen.I1(inst, endianness)
    return table.concat(table.pack(
        gen.R0(inst, endianness),
        string.pack(endianness .. 'I4', inst.immediate)
    ))
end

function gen.I2(inst, endianness)
    return table.concat(table.pack(
        gen.R1(inst, endianness),
        string.pack(endianness .. 'I4', inst.immediate)
    ))
end

function gen.I3(inst, endianness)
    return table.concat(table.pack(
        gen.R2(inst, endianness),
        string.pack(endianness .. 'I4', inst.immediate)
    ))
end

function gen.I4(inst, endianness)
    return table.concat(table.pack(
        gen.R3(inst, endianness),
        string.pack(endianness .. 'I4', inst.immediate)
    ))
end

function gen.I5(inst, endianness)
    return table.concat(table.pack(
        gen.R4(inst, endianness),
        string.pack(endianness .. 'I4', inst.immediate)
    ))
end

function gen.J(inst, endianness)
    return table.concat(table.pack(
        string.pack(endianness .. 'I4', inst.opcode << 25
        | inst.condition << 20),
        string.pack(endianness .. 'I4', inst.address)
    ))
end

function gen.RJ(inst, endianness)
    return string.pack(endianness .. 'I4', inst.opcode << 25 |
    inst.condition << 20 | inst.offset)
end

function gen.IJ(inst, endianness)
    return string.pack(endianness .. 'I4', inst.opcode << 25
    | inst.condition << 20 | inst.ry << 13 | inst.rz << 7)
end

function gen.M(inst, endianness)
    return string.pack(endianness .. 'I4', inst.opcode << 25 | inst.rx << 19
    | inst.ry << 13 | inst.j << 8 | inst.k << 3)
end

return gen
