local arch = require 'arch'
local utils = require 'utils'

local directives = require 'directives'
local assemble = require 'assemble'

-- TODO: Move this verification to the gen.<format> functions
local function dereference_symbols(symbol_table, block)
    local undefined = {}
    for _, key in ipairs({'immediate', 'address', 'offset'}) do
        local symbol_name = type(block[key]) == 'string' and block[key]
        if symbol_name and symbol_table[symbol_name] then
            if type(symbol_table[symbol_name].value == 'number') then
                block[key] = symbol_table[symbol_name].value
            else
                return false, 'error', 'Symbol %q should have an integer value.'
            end
        elseif symbol_name and symbol_table[symbol_name] == nil then
            table.insert(undefined, symbol_name)
        end
    end

    return #undefined == 0, 'success', nil, undefined
end

local handle_block = {}

function handle_block.directive(section, symbol_table, block)
    if type(directives[block.name]) == 'function' then
        return directives[block.name](section, symbol_table, block)
    else
        return nil, 'error', ('Invalid directive %q.')
        :format(tostring(block.name))
    end
end

function handle_block.label(section, symbol_table, block)
    if symbol_table[block.name] == nil then
        symbol_table[block.name] = {
            type = 'label',
            value = section.size
        }
        return nil, 'success'
    else
        return nil, 'error', ("Can't create label %q. Symbol already defined")
        :format(block.name)
    end
end

function handle_block.instruction(section, symbol_table, block, options)
    -- Takes the instruction format and size
    local format = arch.instructions.byOpcode[block.opcode].format
    local size = arch.instructions.formats[format].size

    if(type(assemble[format]) == 'function') then
        local dref, status, msg = dereference_symbols(symbol_table, block)
        if status ~= 'success' then return nil, status, msg end

        if dref
        then section:append(assemble[format](block, options.endianness), size)
        else section:append(block, size) end
    else
        return nil, 'error', ('Unknown instruction format: %q')
        :format(tostring(format))
    end

    return nil, 'success'
end

-- Empty section "prototype". This will be shallow copied to create new sections
local _section = {'', size = 0}
function _section.append(section, data, size)
    if type(section[#section]) == 'string' and type(data) == 'string' then
        section[#section] = section[#section] .. data
        size = size or #data
    else
        table.insert(section, data)
    end
    section.size = section.size + size
end

local function codegen(parsed, options)
    local active_section = '.text'
    local sections = {[active_section] = utils.shallow_copy(_section)}
    local symbol_table = {}
    local error = false

    -- First pass
    for _, block in ipairs(parsed) do
        if(handle_block[block.type]) then
            local new_active_section, status, msg =
            handle_block[block.type](sections[active_section], symbol_table, block, options)

            if(new_active_section) then
                active_section = new_active_section
                if sections[active_section] == nil then
                    sections[active_section] = utils.shallow_copy(_section)
                end
            end

            if status == 'error' then
                io.stderr:write(('Error: %s\n'):format(msg))
                error = true
            elseif status == 'warning' then
                io.stderr:write(('Warning %s\n'):format(msg))
            end
        else
            io.stderr:write(("Can't handle block:\n%sSkipping.")
            :format(utils.sinspect(block)))
        end
    end

    -- Second pass
    for s, section in pairs(sections) do
        for b, block in ipairs(section) do
            if type(block) == 'table' then
                local format = arch.instructions.byOpcode[block.opcode].format
                local dref, status, msg, undefined =
                dereference_symbols(symbol_table, block)

                if status ~= 'success' then return nil, status, msg end
                if #undefined > 0 then
                    io.stderr:write(('Error: Undefined references to: %s\n')
                    :format(table.concat(undefined, ', ')))
                    error = true
                else
                    sections[s][b] = assemble[format](block)
                end
            end
        end

        if not error then sections[s] = table.concat(section) end
    end

    if error then return nil else return sections, symbol_table end
end

return codegen
