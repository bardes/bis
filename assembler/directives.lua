local utils = require 'utils'
local directives = {}

-- TODO Abstract argument chacking inta a function

function directives.text(section, symbol_table, block)
    if block.args then
        return '.text', 'warning', 'This directive takes no arguments'
    else
        return '.text', 'success'
    end
end

function directives.data(section, symbol_table, block)
    if block.args then
        return '.data', 'warning', 'This directive takes no arguments'
    else
        return '.data', 'success'
    end
end

-- TODO Verify section name length
function directives.section(section, symbol_table, block)
    if not block.args or type(block.args[1]) ~= 'string' then
        return nil, 'error', 'Missing string argument.'
    end
    if #block.args ~= 1 then
        return block.args[1], 'warning', 'This directive takes exactly one string argument.'
    else
        return block.args[1], 'success'
    end
end

function directives.skip(section, symbol_table, block)
    if not block.args or type(block.args[1]) ~= 'number' then
        return nil, 'error', 'Missing numeric argument.'
    end

    local size = block.args[1]
    section:append(string.pack(('>c%d'):format(size), ''))

    if #block.args ~= 1 then
        return nil, 'warning', 'This directive takes exactly one numeric argument.'
    else
        return nil, 'success'
    end
end

function directives.string(section, symbol_table, block)
    if not block.args or type(block.args[1]) ~= 'string' then
        return nil, 'error', 'Missing string argument.'
    end

    local str = block.args[1]
    section:append(string.pack('z', str))

    if #block.args ~= 1 then
        return nil, 'warning', 'This directive takes exactly one string argument.'
    else
        return nil, 'success'
    end
end

return directives
