# Target and version information
TARGET = bis-emulator
VERSION = 0.0.1

# Flags and warnings
WARNINGS = -Wformat -Wall -Wextra -pedantic -Wconversion
LOCAL_CFLAGS = $(WARNINGS) -O3 -march=native -std=c11 $(CFLAGS)
LOCAL_CPPFLAGS = -I$(GEN_DIR) "-D_POSIX_C_SOURCE=(200809L)" $(CPPFLAGS)
LOCAL_LDFLAGS = $(LDFLAGS)

# Directories locations
BIS_DIR = ./bis
EMULATOR_DIR = ./emulator
ASSEMBLER_DIR = ./assembler
BUILD_DIR = ./build
DEP_DIR = $(BUILD_DIR)/.deps
GEN_DIR = $(BUILD_DIR)/.gen

# Architecture description files
ARCH_FILES = $(BIS_DIR)/*.lua

# Include file generator
GEN = $(BIS_DIR)/gen.lua
GEN_TARGETS = registers \
			  instructions \
			  instruction_prototypes \
			  instruction_table \
			  conditions

# Emulator source files
EMULATOR_FILES := $(wildcard $(EMULATOR_DIR)/*.c)
EMULATOR_FILES := $(notdir $(EMULATOR_FILES))

# Lower case variables are "internal"
gen_files = $(addprefix $(GEN_DIR)/, $(addsuffix .gen, $(GEN_TARGETS)))
objs = $(addprefix $(BUILD_DIR)/, $(EMULATOR_FILES:.c=.o))
deps = $(addprefix $(DEP_DIR)/, $(EMULATOR_FILES:.c=.d))
LOCAL_CPPFLAGS += '-DVERSION="$(VERSION)"'
MAKEFLAGS += --no-builtin-rules

.PHONY: all clean cleaner debug profile
.PRECIOUS: $(DEP_DIR)/%.d
.SUFFIXES:

all: $(BUILD_DIR)/$(TARGET) monografia

debug: LOCAL_CFLAGS += -O0 -g
debug: LOCAL_CPPFLAGS += -DDEBUG
debug: all

profile: LOCAL_CFLAGS += -g
profile: all

$(GEN_DIR)/%.gen: $(ARCH_FILES) | $(GEN_DIR)
	$(GEN) $* > $@

$(BUILD_DIR)/$(TARGET): $(objs)
	$(CC) $(LOCAL_CFLAGS) $(LOCAL_LDFLAGS) $^ -o $@
	cp $@ .

$(BUILD_DIR)/%.o: $(EMULATOR_DIR)/%.c | $(BUILD_DIR)
	$(CC) -c $(LOCAL_CPPFLAGS) $(LOCAL_CFLAGS) $< -o $@

$(DEP_DIR)/%.d: $(EMULATOR_DIR)/%.c | $(DEP_DIR) $(gen_files)
	$(CC) $(LOCAL_CPPFLAGS) -MM -MQ $(<:.c=.o) $< > $@

$(BUILD_DIR):
	mkdir $@

$(DEP_DIR): | $(BUILD_DIR)
	mkdir $@

$(GEN_DIR): | $(BUILD_DIR)
	mkdir $@

#TODO: Don't generate deps and gen files only to clean them right after
clean:
	rm -f ./$(BUILD_DIR)/*.o
	rm -f ./$(BUILD_DIR)/$(TARGET)
	rm -f $(gen_files)

cleaner: clean
	rm -rf ./$(BUILD_DIR)

include $(deps)
